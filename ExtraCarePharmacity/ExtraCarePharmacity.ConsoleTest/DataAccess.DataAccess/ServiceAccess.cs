﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExtraCarePharmacity.DataAccess.Interfaces;
using ExtraCarePharmacity.DataAccess.ServiceModels;
using Newtonsoft.Json;

namespace ExtraCarePharmacity.ConsoleTest.DataAccess.DataAccess
{
    class ServiceAccess
    {
        IServiceAccess _serviceAccess = (IServiceAccess)new ExtraCarePharmacity.DataAccess.ServiceAccess();

        public async Task Login()
        {
            
            LoginResult result = await _serviceAccess.Login("new_world_seeker@outlook.com");
            string resultString = JsonConvert.SerializeObject(result);
            Debug.WriteLine("Login() result:");
            Debug.WriteLine(resultString);
        }

        public async Task GetBalance()
        {
            
            BalanceResult result = await _serviceAccess.GetBalance("34NhUHgGGEqEibhYJJn5w");
            string resultString = JsonConvert.SerializeObject(result);
            Debug.WriteLine("GetBalance() result:");
            Debug.WriteLine(resultString);
        }

        public async Task GetInboxes()
        {
            
            InboxResult result = await _serviceAccess.GetInboxes("34NhUHgGGEqEibhYJJn5w");
            string resultString = JsonConvert.SerializeObject(result);
            Debug.WriteLine("GetInboxes() result:");
            Debug.WriteLine(resultString);
        }

        public async Task GetStores()
        {
            
            StoreResult result = await _serviceAccess.GetStores();
            string resultString = JsonConvert.SerializeObject(result);
            Debug.WriteLine("GetStores() result:");
            Debug.WriteLine(resultString);
        }

        public async Task GetTransactions()
        {
            
            LoginResult result = await _serviceAccess.Login("34NhUHgGGEqEibhYJJn5w");
            string resultString = JsonConvert.SerializeObject(result);
            Debug.WriteLine("GetTransactions() result:");
            Debug.WriteLine(resultString);
        }
    }
}
