﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtraCarePharmacity.Common.Event
{
    public class GenericEventArgs<T>: EventArgs
    {
        public T EventData
        { get; private set; }

        public GenericEventArgs(T eventData)
        {
            this.EventData = eventData;
        }
    }
}
