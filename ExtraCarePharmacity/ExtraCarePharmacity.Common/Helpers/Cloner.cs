﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtraCarePharmacity.Common.Helpers
{
    public static class Cloner
    {
        public static ObservableCollection<T> CloneObservableCollection<T>(IList<T> sourceCollection)
        {
            ObservableCollection<T> result = new ObservableCollection<T>();
            foreach(var item in sourceCollection)
            {
                result.Add(item);
            }
            return result;
        }

        public static List<T> Clone<T>(List<T> sourceList)
        {
            List<T> clonedList = new List<T>();
            foreach(var item in sourceList)
            {
                clonedList.Add(item);
            }
            return clonedList;
        }
    }
}
