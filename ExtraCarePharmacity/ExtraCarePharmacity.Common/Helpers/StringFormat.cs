﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtraCarePharmacity.Common.Helpers
{
    public static class StringFormat
    {
        public static string FormatCurrency(int currencyAmount, string intergralNumbersSeperator)
        {
            if (currencyAmount == 0)
                return "0";

            StringBuilder result = new StringBuilder();
            int numberCounter = 0;
            while(currencyAmount > 0)
            {
                result.Append(currencyAmount % 10);
                currencyAmount /= 10;
                ++numberCounter;
                if(numberCounter >= 3)
                {
                    result.Append(intergralNumbersSeperator);
                    numberCounter = 0;
                }
            }
            if (result.Length > 0 && result[result.Length - 1] == '.')
                result.Remove(result.Length - 1, 1);

            //Reverse:
            for(int i = 0; i < result.Length / 2; ++i)
            {
                char temp = result[i];
                result[i] = result[result.Length - i - 1];
                result[result.Length - i - 1] = temp;
            }

            return result.ToString();
        }
    }
}
