﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExtraCarePharmacity.BusinessLogic.BusinessModels;
using ExtraCarePharmacity.Common.BaseTypes;
using Kaliko;


namespace ExtraCarePharmacity.Presentation.BindingModels
{
    public class UIStore: BindableBase
    {
        string _id;
        public string Id
        {
            get { return _id; }
            private set { SetProperty(ref _id, value); }
        }

        string _name;
        public string Name
        {
            get { return _name; }
            private set { SetProperty(ref _name, value); }
        }

        string _address;
        public string Address
        {
            get { return _address; }
            private set { SetProperty(ref _address, value); }
        }

        double _latitude;
        public double Latitude
        {
            get { return _latitude; }
            set { SetProperty(ref _latitude, value); }
        }

        double _longitude;
        public double Longitude
        {
            get { return _longitude; }
            set { SetProperty(ref _longitude, value); }
        }

        string _openingTime;
        public string OpeningTime
        {
            get { return _openingTime; }
            private set { SetProperty(ref _openingTime, value); }
        }

        string _closingTime;
        public string ClosingTime
        {
            get { return _closingTime; }
            private set { SetProperty(ref _closingTime, value); }
        }

        Store _store;
        public Store Store
        {
            get { return _store; }
            private set { SetProperty(ref _store, value); }
        }

        bool _isMarked = false;
        public bool IsMarked
        {
            get { return _isMarked; }
            set { SetProperty(ref _isMarked, value); }
        }

        double _distance;
        public double Distance
        {
            get { return _distance; }
            set { SetProperty(ref _distance, value); }
        }

        public UIStore(Store store)
        {
            this.Id = store.Id;
            this.Name = store.Name;
            this.Address = store.Address;
            try
            {
                this.Latitude = double.Parse(store.Latitude);
                this.Longitude = double.Parse(store.Longitude);
            }
            catch (Exception e)
            {
                Debug.WriteLine(string.Format("UIStore.From(...): Can't parse latitude and longtitude with value ({0}, {1}) to double.", store.Latitude, store.Longitude));
                Debug.WriteLine(e.Message);
            }
            this.OpeningTime = store.OpeningTime;
            this.ClosingTime = store.ClosingTime;
            this.Store = store;
        }
    }
}
