﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExtraCarePharmacity.BusinessLogic.BusinessModels;
using ExtraCarePharmacity.Common.BaseTypes;
using ExtraCarePharmacity.Presentation.BaseTypes;

namespace ExtraCarePharmacity.Presentation.BindingModels
{
    class BindableBalance: BindableBase
    {
        string _balanceType;
        public string BalanceType
        {
            get { return _balanceType; }
            set { SetProperty(ref _balanceType, value); }
        }

        DateTime _recentlyRebalancedDate;
        public DateTime RecentlyRebalancedDate
        {
            get { return _recentlyRebalancedDate; }
            set { SetProperty(ref _recentlyRebalancedDate, value); }
        }

        decimal _cumulative;
        public decimal Cumulative
        {
            get { return _cumulative; }
            set { SetProperty(ref _cumulative, value); }
        }

        int _numOfDeals;
        public int NumOfDeals
        {
            get { return _numOfDeals; }
            set { SetProperty(ref _numOfDeals, value); }
        }

        public void Assign(Balance balance)
        {
            this.BalanceType = balance.BalanceType;
            this.RecentlyRebalancedDate = balance.RecentlyRebalancedDate;
            this.Cumulative = balance.Cumulative;
            this.NumOfDeals = balance.NumOfDeals;
        }
    }
}
