﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using ExtraCarePharmacity.BusinessLogic.Interfaces;
using ExtraCarePharmacity.Common.BaseTypes;
using ExtraCarePharmacity.DataAccess.Interfaces;
using ExtraCarePharmacity.Presentation.Interfaces;
using Kaliko;

namespace ExtraCarePharmacity.Presentation.BaseTypes
{
    public abstract class ViewModelBase: BindableBase
    {
        protected IKeyValuePairStorage _keyValuePairStorage;
        protected IPageNavigator _pageNavigator;
        protected IDispatcher _dispatcher;

        public ViewModelBase(IKeyValuePairStorage keyValuePairStorage, IDispatcher mainThreadDispatcher)
        {
            _keyValuePairStorage = keyValuePairStorage;
            _dispatcher = mainThreadDispatcher;
        }

        public void SetPageNavigator(IPageNavigator pageNavigator)
        {
            _pageNavigator = pageNavigator;
        }

        public bool Navigate(string pageName, Dictionary<string, string> navigationParams)
        {
            if (_pageNavigator != null)
            {
                _pageNavigator.Navigate(pageName, navigationParams);
                return true;
            }
            else
            {
                Debug.WriteLine("PageNavigator is not set.");
                return false;
            }
        }

        public virtual void OnNavigatedTo(Dictionary<String, String> navigationParams)
        { }

    }
}
