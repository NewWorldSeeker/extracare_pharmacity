﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExtraCarePharmacity.Presentation.Interfaces
{
    public interface IPageNavigator
    {
        bool Navigate(string pageName, Dictionary<string, string> navigationParams);
        bool RemoveBackEntry();
        void ClearBackEntries();
    }
}
