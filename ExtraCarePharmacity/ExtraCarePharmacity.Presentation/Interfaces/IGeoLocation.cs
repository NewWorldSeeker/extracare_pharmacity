﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExtraCarePharmacity.Common.BaseTypes;

namespace ExtraCarePharmacity.Presentation.Interfaces
{
    public interface IGeoLocation
    {
        double Latitude { get; set; }
        double Longitude { get; set; }
    }

    public class GeoLocation: IGeoLocation
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
