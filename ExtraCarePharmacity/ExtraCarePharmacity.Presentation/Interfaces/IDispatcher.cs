﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtraCarePharmacity.Presentation.Interfaces
{
    public interface IDispatcher
    {
        void BeginInvoke(Action action);
    }
}
