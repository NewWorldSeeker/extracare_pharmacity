﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExtraCarePharmacity.Common.BaseTypes;

namespace ExtraCarePharmacity.Presentation.Interfaces
{
    public interface IGeoLocator
    {
        Task<IGeoLocation> GetLocationAsync(TimeSpan maxCacheAge, TimeSpan timeOut);
    }
}
