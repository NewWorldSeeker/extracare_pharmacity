﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using ExtraCarePharmacity.BusinessLogic;
using ExtraCarePharmacity.BusinessLogic.Interfaces;
using ExtraCarePharmacity.Common.MVVMSupport;
using ExtraCarePharmacity.DataAccess.Interfaces;
using ExtraCarePharmacity.Presentation.BaseTypes;
using ExtraCarePharmacity.Presentation.Interfaces;
using ExtraCarePharmacity.Common.Event;
using ExtraCarePharmacity.BusinessLogic.BusinessModels;

namespace ExtraCarePharmacity.Presentation.ViewModels
{
    public class SignUpPageViewModel: ViewModelBase
    {
        public event EventHandler<GenericEventArgs<string>> ShowMessage;
        void OnShowMessage(string message)
        {
            if (ShowMessage != null)
                ShowMessage(this, new GenericEventArgs<string>(message));
        }

        ICoreService _coreService;

        string _fullName;
        public string FullName
        {
            get { return _fullName; }
            set { SetProperty(ref _fullName, value); }
        }

        string _phoneNumber;
        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set { SetProperty(ref _phoneNumber, value); }
        }

        string _email;
        public string Email
        {
            get { return _email; }
            set { SetProperty(ref _email, value); }
        }

        string _address;
        public string Address
        {
            get { return _address; }
            set { SetProperty(ref _address, value); }
        }

        string _city;
        public string City
        {
            get { return _city; }
            set { SetProperty(ref _city, value); }
        }

        public SignUpPageViewModel(IKeyValuePairStorage keyValuePairStorage, IDispatcher dispatcher)
            : base(keyValuePairStorage, dispatcher)
        {
            _coreService = new CoreService(keyValuePairStorage);

        }

        public override void OnNavigatedTo(Dictionary<string, string> navigationParams)
        {

            base.OnNavigatedTo(navigationParams);
        }

        #region Commands
        ICommand _cancelCommand;
        public ICommand CancelCommand
        {
            get
            {
                if(_cancelCommand == null)
                {
                    _cancelCommand = new DelegateCommand((param) =>
                    {
                        Navigate("SignInPage", new Dictionary<string, string>());
                    });
                }

                return _cancelCommand;
            }
        }

        ICommand _signUpCommand;
        public ICommand SignUpCommand
        {
            get
            {
                if(_signUpCommand == null)
                {
                    _signUpCommand = new DelegateCommand(async (param) =>
                    {
                        if(string.IsNullOrWhiteSpace(FullName))
                        {
                            OnShowMessage("Tên không được để trống!");
                            return;
                        }
                        if(string.IsNullOrWhiteSpace(PhoneNumber))
                        {
                            OnShowMessage("Số điện thoại không được để trống!");
                            return;
                        }
                        if(string.IsNullOrWhiteSpace(Email))
                        {
                            OnShowMessage("Email không được để trống!");
                            return;
                        }
                        if(string.IsNullOrWhiteSpace(Address))
                        {
                            OnShowMessage("Địa chỉ không được để trống!");
                            return;
                        }
                        if(string.IsNullOrWhiteSpace(City))
                        {
                            OnShowMessage("Thành phố không được để trống!");
                            return;
                        }

                        bool signUpResult = await _coreService.SignUp(new SignUpData { CustomerName = this.FullName, Phone = this.PhoneNumber, Email = this.Email, Street = this.Address, City = this.City });
                        if(signUpResult == true)
                        {
                            OnShowMessage("Bạn đã đăng kí thành công tài khoản trong hệ thống của chúng tôi! Bây giờ, bạn có thể đăng nhập vào ứng dụng bằng email vừa đăng kí!");
                            Dictionary<string, string> navigationParams = new Dictionary<string, string>();
                            navigationParams.Add("email", this.Email);
                            Navigate("SignInPage", navigationParams);
                        }
                        else
                        {
                            OnShowMessage("Vui lòng kiểm tra lại thông tin vừa nhập và kết nối mạng!");
                        }
                    });
                }

                return _signUpCommand;
            }
        }
        #endregion
    }
}
