﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using ExtraCarePharmacity.BusinessLogic;
using ExtraCarePharmacity.BusinessLogic.BusinessModels;
using ExtraCarePharmacity.BusinessLogic.Interfaces;
using ExtraCarePharmacity.Common.MVVMSupport;
using ExtraCarePharmacity.DataAccess.Interfaces;
using ExtraCarePharmacity.Presentation.BaseTypes;
using ExtraCarePharmacity.Presentation.Interfaces;

namespace ExtraCarePharmacity.Presentation.ViewModels
{
    public class TransactionsPageViewModel: ViewModelBase
    {
        ICoreService _coreService;

        #region Binding properties
        ObservableCollection<Transaction> _transactions;
        public ObservableCollection<Transaction> Transactions
        {
            get { return _transactions; }
            set { SetProperty(ref _transactions, value); }
        }

        string _notification;
        public string Notification
        {
            get { return _notification; }
            set { SetProperty(ref _notification, value); }
        }
        #endregion

        public TransactionsPageViewModel(IKeyValuePairStorage keyValuePairStorage, IDispatcher dispatcher)
            : base(keyValuePairStorage, dispatcher)
        {
            _coreService = new CoreService(keyValuePairStorage);

            _coreService.DataUpdated += (sender, e) =>
                {
                    if (e.DataPropertyName == "Transactions")
                    {
                        _dispatcher.BeginInvoke(() =>
                            {
                                if (_coreService.Transactions == null)
                                {
                                    if(_coreService.CheckIsConnectionFailed())
                                    {
                                        Notification = "Không thể kết nối tới máy chủ, xin hãy kiểm tra lại đường truyền!";
                                    }
                                    else if (!_coreService.CheckIsConfirm())
                                    {
                                        Notification = "Bạn cần xác nhận email để xem được giao dịch!";
                                    }
                                }
                                else if(_coreService.Transactions.Count == 0)
                                {
                                    Notification = "Không có giao dịch nào!";
                                }
                                else
                                {
                                    Notification = null;
                                }
                                if (_coreService.Transactions != null && _coreService.Transactions.Count > 0)
                                {
                                    Transactions = Convert(_coreService.Transactions);
                                }
                            });
                    }
                };

        }

        public override void OnNavigatedTo(Dictionary<string, string> navigationParams)
        {
            List<Transaction> transactions = _coreService.Transactions;
            if(transactions != null && transactions.Count > 0)
                Transactions = Convert(transactions);

            Notification = "Đang thực hiện tải dữ liệu giao dịch";

            _coreService.UpdateTransactions();
            
            base.OnNavigatedTo(navigationParams);
        }

        private static ObservableCollection<Transaction> Convert(List<Transaction> transactions)
        {
            if (transactions == null)
                return null;

            ObservableCollection<Transaction> tempTransactions = new ObservableCollection<Transaction>();
            foreach (var item in transactions)
            {
                tempTransactions.Add(item);
            }
            return tempTransactions;
        }

        #region Commands
        ICommand _transactionSelectedCommand;
        public ICommand TransactionSelectedCommand
        {
            get
            {
                if (_transactionSelectedCommand == null)
                {
                    _transactionSelectedCommand = new DelegateCommand((param) =>
                    {
                        if (param != null)
                        {
                            string transactionId = ((Transaction)param).TransactionId;
                            Dictionary<string, string> navigationParams = new Dictionary<string, string>();
                            navigationParams.Add("transactionId", transactionId);
                            Navigate("TransactionDetailsPage", navigationParams);
                        }
                    });
                }

                return _transactionSelectedCommand;
            }
        }
        #endregion
    }
}
