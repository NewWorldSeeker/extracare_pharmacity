﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExtraCarePharmacity.BusinessLogic;
using ExtraCarePharmacity.BusinessLogic.Interfaces;
using ExtraCarePharmacity.DataAccess.Interfaces;
using ExtraCarePharmacity.Presentation.BaseTypes;
using ExtraCarePharmacity.Presentation.Interfaces;
using System.Collections.ObjectModel;
using ExtraCarePharmacity.BusinessLogic.BusinessModels;
using ExtraCarePharmacity.Common.Helpers;

namespace ExtraCarePharmacity.Presentation.ViewModels
{
    public class ReminderPageViewModel: ViewModelBase
    {
        ICoreService _coreService;

        #region Binding properties
        ObservableCollection<Reminder> _reminders;
        public ObservableCollection<Reminder> Reminders
        {
            get { return _reminders; }
            set { SetProperty(ref _reminders, value); }
        }

        string _notification;
        public string Notification
        {
            get { return _notification; }
            set { SetProperty(ref _notification, value); }
        }
        #endregion

        public ReminderPageViewModel(IKeyValuePairStorage keyValuePairStorage, IDispatcher dispatcher)
            : base(keyValuePairStorage, dispatcher)
        {
            _coreService = new CoreService(keyValuePairStorage);

            Notification = "Không có nhắc nhở nào.";

            _coreService.DataUpdated += (sender, args) =>
                {
                    dispatcher.BeginInvoke(() =>
                        {
                            if (_coreService.Reminders != null && _coreService.Reminders.Count > 0)
                            {
                                Reminders = Cloner.CloneObservableCollection(_coreService.Reminders);
                                Notification = "";
                            }
                            else
                            {
                                Notification = "Không có nhắc nhở nào.";
                            }
                        });
                };
        }

        public override void OnNavigatedTo(Dictionary<string, string> navigationParams)
        {
            if(_coreService.Reminders != null && _coreService.Reminders.Count > 0)
            {
                Reminders = Cloner.CloneObservableCollection(_coreService.Reminders);
                Notification = "";
            }

            _coreService.UpdateReminders();

            base.OnNavigatedTo(navigationParams);
        }
    }
}
