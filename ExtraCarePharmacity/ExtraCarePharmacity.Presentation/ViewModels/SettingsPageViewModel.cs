﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExtraCarePharmacity.BusinessLogic;
using ExtraCarePharmacity.BusinessLogic.BusinessModels;
using ExtraCarePharmacity.BusinessLogic.Interfaces;
using ExtraCarePharmacity.DataAccess.Interfaces;
using ExtraCarePharmacity.Presentation.BaseTypes;
using ExtraCarePharmacity.Presentation.Interfaces;
using ExtraCarePharmacity.Common.MVVMSupport;
using System.Windows.Input;

namespace ExtraCarePharmacity.Presentation.ViewModels
{
    public class SettingsPageViewModel: ViewModelBase
    {
        ICoreService _coreService;

        string aboutUrl = "http://nhathuoconline.vn/gioi-thieu.html";
        string homeUrl = "http://nhathuoconline.vn/";
        string termsUrl = "http://nhathuoconline.vn/thoa-thuan-quy-dinh.html";
        string policyUrl = "http://nhathuoconline.vn/dieu-khoan-su-dung.html";
        string aboutTitle = "VỀ CHÚNG TÔI";
        string homeTitle = "TRANG CHỦ";
        string termsTitle = "ĐIỀU KHOẢN SỬ DỤNG";
        string policyTitle = "CHÍNH SÁCH BẢO MẬT";

        static readonly string NotificationTaskAgentName = "InboxNotificationTaskAgent";

        //Binding properties:
        string _email;
        string _version;
        public string Email
        {
            get { return _email; }
            set { SetProperty(ref _email, value); }
        }
        public string Version
        {
            get { return _version; }
            set { SetProperty(ref _version, value); }
        }

        SessionInfo _sessionInfo;
        public SessionInfo SessionInfo
        {
            get { return _sessionInfo; }
            set { SetProperty(ref _sessionInfo, value); }
        }

        bool _isNotificationEnabled = true;
        public bool IsNotificationEnabled
        {
            get { return _isNotificationEnabled; }
            set { SetProperty(ref _isNotificationEnabled, value); }
        }

        ICommand _checkedInboxCommand;
        public ICommand CheckedInboxCommand
        {
            get
            {
                return _checkedInboxCommand;
            }
        }

        //Handle About_tap event:
        ICommand _aboutTapCommand;
        public ICommand AboutTapCommand
        {
            get
            {
                if (_aboutTapCommand == null)
                {
                    _aboutTapCommand = new DelegateCommand((param) =>
                    {
                        //Code to handle About_tap event:
                        Dictionary<string, string> navigationParams = new Dictionary<string, string>();
                        navigationParams.Add("link", aboutUrl);
                        navigationParams.Add("title", aboutTitle);
                        Navigate("WebBrowserPage", navigationParams);
                    });
                }
                return _aboutTapCommand;
            }
        }

        ICommand _homeTapCommand;
        public ICommand HomeTapCommand
        {
            get
            {
                if (_homeTapCommand == null)
                {
                    _homeTapCommand = new DelegateCommand((param) =>
                    {
                        Dictionary<string, string> navigationParams = new Dictionary<string, string>();
                        navigationParams.Add("link", homeUrl);
                        navigationParams.Add("title", homeTitle);
                        Navigate("WebBrowserPage", navigationParams);
                    });
                }
                return _homeTapCommand;
            }
        }

        ICommand _termsTapCommand;
        public ICommand TermsTapCommand
        {
            get
            {
                if (_termsTapCommand == null)
                {
                    _termsTapCommand = new DelegateCommand((param) =>
                    {
                        Dictionary<string, string> navigationParams = new Dictionary<string, string>();
                        navigationParams.Add("link", termsUrl);
                        navigationParams.Add("title", termsTitle);
                        Navigate("WebBrowserPage", navigationParams);
                    });
                }
                return _termsTapCommand;
            }
        }

        ICommand _policyTapCommand;
        public ICommand PolicyTapCommand
        {
            get
            {
                if (_policyTapCommand == null)
                {
                    _policyTapCommand = new DelegateCommand((param) =>
                    {
                        Dictionary<string, string> navigationParams = new Dictionary<string, string>();
                        navigationParams.Add("link", policyUrl);
                        navigationParams.Add("title", policyTitle);
                        Navigate("WebBrowserPage", navigationParams);
                    });
                }
                return _policyTapCommand;
            }
        }

        ICommand _signOutTapCommand;
        public ICommand SignOutTapCommand
        {
            get
            {
                if (_signOutTapCommand == null)
                {
                    _signOutTapCommand = new DelegateCommand((param) =>
                    {
                        _coreService.Logout();
                        Dictionary<string, string> navigationParams = new Dictionary<string, string>();
                        Navigate("SignInPage", navigationParams);
                    });
                }
                return _signOutTapCommand;
            }
        }

        public SettingsPageViewModel(IKeyValuePairStorage keyValuePairStorage, IDispatcher dispatcher)
            : base(keyValuePairStorage, dispatcher)
        {
            _coreService = new CoreService(keyValuePairStorage);
        }

        public override void OnNavigatedTo(Dictionary<string, string> navigationParams)
        {
            SessionInfo = _coreService.SessionInfo;

            if(SessionInfo != null)
            {
                Email = SessionInfo.Email;
            }

            //Code to handle load data:
            Version = "Phiên bản 0.9 beta";
            base.OnNavigatedTo(navigationParams);
        }
    }
}
