﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using ExtraCarePharmacity.BusinessLogic;
using ExtraCarePharmacity.BusinessLogic.Interfaces;
using ExtraCarePharmacity.Common.MVVMSupport;
using ExtraCarePharmacity.DataAccess.Interfaces;
using ExtraCarePharmacity.Presentation.BaseTypes;
using ExtraCarePharmacity.Presentation.Interfaces;

namespace ExtraCarePharmacity.Presentation.ViewModels
{
    public class SignInPageViewModel: ViewModelBase
    {
        ICoreService _coreService;

        public event EventHandler<EventArgs> SignInError;

        bool _isSignInInProcess = false;
        public bool IsSignInInProcess
        {
            get { return _isSignInInProcess; }
            set { SetProperty(ref _isSignInInProcess, value); }
        }

        string _emailOrPhoneNumber;
        public string EmailOrPhoneNumber
        {
            get { return _emailOrPhoneNumber; }
            set { SetProperty(ref _emailOrPhoneNumber, value); }
        }

        public SignInPageViewModel(IKeyValuePairStorage keyValuePairStorage, IDispatcher dispatcher)
            : base(keyValuePairStorage, dispatcher)
        {
            _coreService = new CoreService(keyValuePairStorage);

        }

        public override void OnNavigatedTo(Dictionary<string, string> navigationParams)
        {
            base.OnNavigatedTo(navigationParams);

            _pageNavigator.ClearBackEntries();
        }

        ICommand _signInCommand;
        public ICommand SignInCommand
        {
            get 
            {
                if(_signInCommand == null)
                {
                    _signInCommand = new DelegateCommand( async (param) =>
                    {
                        if (string.IsNullOrEmpty(EmailOrPhoneNumber))
                            return;

                        IsSignInInProcess = true;
                        if(await _coreService.Login(EmailOrPhoneNumber))
                        {
                            Navigate("WelcomePage", new Dictionary<string, string>());
                        }
                        else
                        {
                            if(SignInError != null)
                            {
                                SignInError(this, new EventArgs());
                            }
                        }
                        IsSignInInProcess = false;
                    });
                }

                return _signInCommand;
            }
        }

        ICommand _signUpCommand;
        public ICommand SignUpCommand
        {
            get
            {
                if(_signUpCommand == null)
                {
                    _signUpCommand = new DelegateCommand((param) =>
                    {
                        Navigate("SignUpPage", new Dictionary<string, string>());
                    });
                }

                return _signUpCommand;
            }
        }
    }
}
