﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using ExtraCarePharmacity.BusinessLogic;
using ExtraCarePharmacity.BusinessLogic.BusinessModels;
using ExtraCarePharmacity.BusinessLogic.Interfaces;
using ExtraCarePharmacity.Common.BaseTypes;
using ExtraCarePharmacity.Common.Helpers;
using ExtraCarePharmacity.Common.MVVMSupport;
using ExtraCarePharmacity.DataAccess.Interfaces;
using ExtraCarePharmacity.Presentation.BaseTypes;
using ExtraCarePharmacity.Presentation.BindingModels;
using ExtraCarePharmacity.Presentation.Interfaces;

namespace ExtraCarePharmacity.Presentation.ViewModels
{
    public class StoresPageViewModel: ViewModelBase
    {
        public event EventHandler<EventArgs> StoreLoadingFailed;
        public event EventHandler<EventArgs> LocationGettingFailed;

        ICoreService _coreService;
        IGeoLocator _geoLocator;

        bool _isFirstLocationSet = false;

        //Binding properties:
        ObservableCollection<UIStore> _stores;
        public ObservableCollection<UIStore> Stores
        {
            get { return _stores; }
            set { SetProperty(ref _stores, value); }
        }

        UIStore _focusedStore;
        public UIStore FocusedStore
        {
            get { return _focusedStore; }
            set { SetProperty(ref _focusedStore, value); }
        }

        LatitudeLongitude _userCoordinate;
        public LatitudeLongitude UserCoordinate
        {
            get { return _userCoordinate; }
            set { SetProperty(ref _userCoordinate, value);}
        }

        IGeoLocation _mapViewLocation;
        public IGeoLocation MapViewLocation
        {
            get { return _mapViewLocation; }
            set { SetProperty(ref _mapViewLocation, value); }
        }

        ICommand _storeTapCommand;
        public ICommand StoreTapCommand
        {
            get
            {
                if(_storeTapCommand == null)
                {
                    _storeTapCommand = new DelegateCommand((param) =>
                    {
                        string storeId = (string)param;
                        UIStore focusedStore = null;
                        foreach (UIStore store in Stores)
                        {
                            if (store.IsMarked == true)
                                store.IsMarked = false;
                            if (store.Id == storeId)
                            {
                                focusedStore = store;
                            }
                        }
                        if (focusedStore != null)
                        {
                            focusedStore.IsMarked = true;
                            FocusedStore = focusedStore;
                            MapViewLocation = new GeoLocation { Latitude = focusedStore.Latitude, Longitude = focusedStore.Longitude };
                        }
                    });
                }

                return _storeTapCommand;
            }
        }


        public StoresPageViewModel(IKeyValuePairStorage keyValuePairStorage, IDispatcher dispatcher, IGeoLocator geoLocator)
            : base(keyValuePairStorage, dispatcher)
        {
            _coreService = new CoreService(keyValuePairStorage);
            _geoLocator = geoLocator;

            _userCoordinate = null;
            _stores = null;

            _coreService.DataUpdated += (sender, args) =>
                {
                    if (args.DataPropertyName == "Stores")
                    {
                        dispatcher.BeginInvoke(() =>
                            {
                                List<Store> stores = _coreService.Stores;
                                if (stores != null)
                                {
                                    Stores = CloneToUIStoreCollection(stores);
                                    GetAndSetUserCoordinate(true);
                                }
                                else
                                {
                                    if (StoreLoadingFailed != null)
                                        StoreLoadingFailed(this, new EventArgs());
                                    return;
                                }

                                if(UserCoordinate != null)
                                {
                                    RecalculateStoreDistance(new GeoLocation { Latitude = UserCoordinate.Latitude, Longitude = UserCoordinate.Longitude }, Stores);
                                    if(!_isFirstLocationSet)
                                    {
                                        SetMapViewToNearestStore();
                                    }
                                }
                            });
                    }
                };
        }

        public override void OnNavigatedTo(Dictionary<string, string> navigationParams)
        {
            List<Store> stores = _coreService.Stores;
            if (stores != null)
            {
                Stores = CloneToUIStoreCollection(stores);
            }

            _coreService.UpdateStores();

            //TODO: Check for GeoLocator permission

            base.OnNavigatedTo(navigationParams);
        }

        static ObservableCollection<UIStore> CloneToUIStoreCollection(List<Store> storeList)
        {
            ObservableCollection<UIStore> result = new ObservableCollection<UIStore>();
            foreach(var item in storeList)
            {
                result.Add(new UIStore(item));
            }
            return result;
        }

        async void GetAndSetUserCoordinate(bool setViewLocation = false)
        {
            IGeoLocation location = null;
            int getLocationCount = 0;
            do
            {
                location = await _geoLocator.GetLocationAsync(TimeSpan.FromMinutes(30), TimeSpan.FromSeconds(10));
                getLocationCount++;
                if(getLocationCount >= 2)
                {
                    break;
                }
            } 
            while (location == null);

            if (location == null)
            {
                if(LocationGettingFailed != null)
                {
                    LocationGettingFailed(this, new EventArgs());
                }
                return;
            }
            if(Stores == null)
                return;

            RecalculateStoreDistance(location, Stores);

            UserCoordinate = new LatitudeLongitude { Latitude = location.Latitude, Longitude = location.Longitude };
            if (!_isFirstLocationSet)
                SetMapViewToNearestStore();
        }

        void SetMapViewToNearestStore()
        {
            if (Stores.Count > 0)
            {
                UIStore nearestStore = Stores[0];
                for (int i = 1; i < Stores.Count; ++i)
                {
                    if (Stores[i].Distance < nearestStore.Distance)
                    {
                        nearestStore = Stores[i];
                    }
                }
                MapViewLocation = new GeoLocation { Latitude = nearestStore.Latitude, Longitude = nearestStore.Longitude };
                FocusedStore = nearestStore;
                FocusedStore.IsMarked = true;
            }
        }

        void RecalculateStoreDistance(IGeoLocation userLocation, ObservableCollection<UIStore> stores)
        {
            foreach (UIStore store in Stores)
            {
                var R = 6371; // km
                var lat1 = userLocation.Latitude;
                var lon1 = userLocation.Longitude;
                var lat2 = store.Latitude;
                var lon2 = store.Longitude;
                var dLat = ((lat2 - lat1) * Math.PI) / 180;
                var dLon = ((lon2 - lon1) * Math.PI) / 180;
                lat1 = (lat1 * Math.PI) / 180;
                lat2 = (lat2 * Math.PI) / 180;

                var a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
                        Math.Sin(dLon / 2) * Math.Sin(dLon / 2) * Math.Cos(lat1) * Math.Cos(lat2);
                var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
                var distance = R * c;

                store.Distance = distance;
            }
        }
    }

    public class LatitudeLongitude: BindableBase
    {
        double _latitude;
        public double Latitude
        {
            get { return _latitude; }
            set { SetProperty(ref _latitude, value); }
        }

        double _longitude;
        public double Longitude
        {
            get { return _longitude; }
            set { SetProperty(ref _longitude, value); }
        }
    }
}
