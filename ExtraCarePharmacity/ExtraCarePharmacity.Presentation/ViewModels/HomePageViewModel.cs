﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using ExtraCarePharmacity.BusinessLogic;
using ExtraCarePharmacity.BusinessLogic.BusinessModels;
using ExtraCarePharmacity.BusinessLogic.Interfaces;
using ExtraCarePharmacity.Common.MVVMSupport;
using ExtraCarePharmacity.DataAccess.Interfaces;
using ExtraCarePharmacity.Presentation.BaseTypes;
using ExtraCarePharmacity.Presentation.Interfaces;

namespace ExtraCarePharmacity.Presentation.ViewModels
{
    public class HomePageViewModel: ViewModelBase
    {
        ICoreService _coreService;

        #region Binding properties
        Balance _balance;
        public Balance Balance
        {
            get { return _balance; }
            set { SetProperty(ref _balance, value); }
        }

        SessionInfo _sessionInfo;
        public SessionInfo SessionInfo
        {
            get { return _sessionInfo; }
            set { SetProperty(ref _sessionInfo, value); }
        }

        DateTime _currentDate;
        public DateTime CurrentDate
        {
            get { return _currentDate; }
            set { SetProperty(ref _currentDate, value); }
        }

        string _notification;
        public string Notification
        {
            get { return _notification; }
            set { SetProperty(ref _notification, value); }
        }
        #endregion

        public HomePageViewModel(IKeyValuePairStorage keyValuePairStorage, IDispatcher dispatcher)
            : base(keyValuePairStorage, dispatcher)
        {
            _coreService = new CoreService(keyValuePairStorage);
            CurrentDate = DateTime.Now;

            this.PropertyChanged += (sender, args) =>
                {
                    if(args.PropertyName == "Balance")
                    {
                    }
                };
        }

        public override void OnNavigatedTo(Dictionary<string, string> navigationParams)
        {
            SessionInfo = _coreService.SessionInfo;
            Balance = _coreService.Balance;
            Notification = "Không thể kết nối tới máy chủ, xin hãy kiểm tra lại đường truyền!";

            //Update balance:
            _coreService.DataUpdated += (sender, args) =>
                {
                    _dispatcher.BeginInvoke(() =>
                        {
                            if(_coreService.Balance == null)
                            {
                                if(_coreService.CheckIsConfirm())
                                {
                                    Notification = "Bạn cần xác nhận email để xem được số dư!";
                                }
                                else
                                {
                                    Notification = "Không thể kết nối tới máy chủ, xin hãy kiểm tra lại đường truyền!";
                                }
                            }
                            else
                            {
                                Notification = "Số dư sẽ được cập nhật sau mỗi 15 phút!";
                            }
                            Balance = _coreService.Balance;
                        });
                };
            _coreService.UpdateBalance();
            //TODO: Add timer to update balance each 15 minutes

            base.OnNavigatedTo(navigationParams);

            _pageNavigator.ClearBackEntries();
        }

        ICommand _qrCodePageNavigationCommand;
        public ICommand QRCodePageNavigationCommand
        {
            get 
            {
                if(_qrCodePageNavigationCommand == null)
                {
                    _qrCodePageNavigationCommand = new DelegateCommand((param) =>
                        {
                            Navigate("QRCodePage", new Dictionary<string, string>());
                        });
                }

                return _qrCodePageNavigationCommand;
            }
        }
    }
}
