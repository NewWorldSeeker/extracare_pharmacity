﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExtraCarePharmacity.BusinessLogic;
using ExtraCarePharmacity.BusinessLogic.Interfaces;
using ExtraCarePharmacity.DataAccess.Interfaces;
using ExtraCarePharmacity.Presentation.BaseTypes;
using ExtraCarePharmacity.Presentation.Interfaces;

namespace ExtraCarePharmacity.Presentation.ViewModels
{
    public class WebBrowserPageViewModel: ViewModelBase
    {
        ICoreService _coreService;

        public WebBrowserPageViewModel(IKeyValuePairStorage keyValuePairStorage, IDispatcher dispatcher)
            : base(keyValuePairStorage, dispatcher)
        {
            _coreService = new CoreService(keyValuePairStorage);

        }

        public override void OnNavigatedTo(Dictionary<string, string> navigationParams)
        {

            base.OnNavigatedTo(navigationParams);
        }
    }
}
