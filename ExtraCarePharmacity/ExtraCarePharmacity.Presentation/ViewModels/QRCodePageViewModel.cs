﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExtraCarePharmacity.BusinessLogic;
using ExtraCarePharmacity.BusinessLogic.BusinessModels;
using ExtraCarePharmacity.BusinessLogic.Interfaces;
using ExtraCarePharmacity.DataAccess.Interfaces;
using ExtraCarePharmacity.Presentation.BaseTypes;
using ExtraCarePharmacity.Presentation.Interfaces;

namespace ExtraCarePharmacity.Presentation.ViewModels
{
    public class QRCodePageViewModel: ViewModelBase
    {
        ICoreService _coreService;
        SessionInfo _sessionInfo;

        string _cardNumber;
        public string CardNumber
        {
            get { return _cardNumber; }
            set { SetProperty(ref _cardNumber, value); }
        }

        Balance _balance;
        public Balance Balance
        {
            get { return _balance; }
            set { SetProperty(ref _balance, value); }
        }

        public QRCodePageViewModel(IKeyValuePairStorage keyValuePairStorage, IDispatcher dispatcher)
            : base(keyValuePairStorage, dispatcher)
        {
            _coreService = new CoreService(keyValuePairStorage);
        }

        public override void OnNavigatedTo(Dictionary<string, string> navigationParams)
        {
            _sessionInfo = _coreService.SessionInfo;
            if(_sessionInfo != null)
            {
                CardNumber = _sessionInfo.CardNumber;
            }

            Balance = _coreService.Balance;

            base.OnNavigatedTo(navigationParams);
        }
    }
}
