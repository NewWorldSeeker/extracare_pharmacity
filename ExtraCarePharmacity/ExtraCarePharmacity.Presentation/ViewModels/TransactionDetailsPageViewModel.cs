﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExtraCarePharmacity.BusinessLogic;
using ExtraCarePharmacity.BusinessLogic.BusinessModels;
using ExtraCarePharmacity.BusinessLogic.Interfaces;
using ExtraCarePharmacity.Common.Helpers;
using ExtraCarePharmacity.Presentation.BaseTypes;
using ExtraCarePharmacity.Presentation.Interfaces;

namespace ExtraCarePharmacity.Presentation.ViewModels
{
    public class TransactionDetailsPageViewModel: ViewModelBase
    {
        ICoreService _coreService;

        #region Binding properties
        ObservableCollection<TransactionDetail> _transactionDetails;
        public ObservableCollection<TransactionDetail> TransactionDetails
        {
            get { return _transactionDetails; }
            set { SetProperty(ref _transactionDetails, value); }
        }

        TransactionDetail _randomTransactionDetail;
        public TransactionDetail RandomTransactionDetail
        {
            get { return _randomTransactionDetail; }
            set { SetProperty(ref _randomTransactionDetail, value); }
        }
        #endregion

        public TransactionDetailsPageViewModel(IKeyValuePairStorage keyValuePairStorage, IDispatcher dispatcher)
            :base(keyValuePairStorage, dispatcher)
        {
            _coreService = new CoreService(keyValuePairStorage);
        }

        public async override void OnNavigatedTo(Dictionary<string, string> navigationParams)
        {
            if(navigationParams.ContainsKey("transactionId"))
            {
                string transactionId = navigationParams["transactionId"];
                List<TransactionDetail> transactionDetails = await _coreService.GetTransactionDetails(transactionId);
                ObservableCollection<TransactionDetail> transactionDetailCollection = Cloner.CloneObservableCollection<TransactionDetail>(transactionDetails);
                TransactionDetails = transactionDetailCollection;
                if(TransactionDetails.Count > 0)
                {
                    //Don't ask me why I'm doing this.
                    //This is because of the bad design of service API.
                    RandomTransactionDetail = TransactionDetails[0];
                }
            }

            base.OnNavigatedTo(navigationParams);
        }
    }
}
