﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExtraCarePharmacity.BusinessLogic;
using ExtraCarePharmacity.BusinessLogic.BusinessModels;
using ExtraCarePharmacity.BusinessLogic.Interfaces;
using ExtraCarePharmacity.DataAccess.Interfaces;
using ExtraCarePharmacity.Presentation.BaseTypes;
using ExtraCarePharmacity.Presentation.Interfaces;

namespace ExtraCarePharmacity.Presentation.ViewModels
{
    public class InboxPageViewModel: ViewModelBase
    {
        ICoreService _coreService;

        List<Inbox> _inboxes;
        int _currentInboxesIndex = 0;

        //Binding properties:
        Inbox _inbox;
        public Inbox Inbox
        {
            get { return _inbox; }
            set { SetProperty(ref _inbox, value); }
        }

        public InboxPageViewModel(IKeyValuePairStorage keyValuePairStorage, IDispatcher dispatcher)
            :base(keyValuePairStorage, dispatcher)
        {
            _coreService = new CoreService(keyValuePairStorage);

            _coreService.DataUpdated += (sender, args) =>
                {
                    if (args.DataPropertyName == "Inboxes")
                    {
                        _dispatcher.BeginInvoke(() =>
                            {
                                UpdateInboxes(_coreService.Inboxes);
                            });
                    }
                };
        }

        public override void OnNavigatedTo(Dictionary<string, string> navigationParams)
        {
            UpdateInboxes(_coreService.Inboxes);

            _coreService.UpdateInboxes();

            base.OnNavigatedTo(navigationParams);
        }

        void UpdateInboxes(List<Inbox> inboxes)
        {
            if (inboxes == null)
                return;

            _inboxes = inboxes;
            if(_inboxes.Count > 0)
            {
                Inbox = _inboxes[0];
            }
        }
    }
}
