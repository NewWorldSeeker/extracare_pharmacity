﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ExtraCarePharmacity.DataAccess.Helpers
{
    public class HttpRequest
    {
        private string _requestUrl;
        Action<HttpResponse> _responseAction;
        string _contentType;

        public HttpRequest(string requestUrl, Action<HttpResponse> responseAction)
        {
            _requestUrl = requestUrl;
            _responseAction = responseAction;
        }

        public void SetContentType(string contentType)
        {
            _contentType = contentType;
        }

        public void DoGetRequest(string getParamsString)
        {
            HttpWebRequest request = WebRequest.CreateHttp(_requestUrl + "?" + getParamsString);
            request.Method = "GET";
            if (_contentType != null)
                request.ContentType = _contentType;
            Task task = Task.Run(() =>
            {
                request.BeginGetResponse(CompleteRequest, request);
            });
        }

        public async void DoPostRequest(string postData)
        {
            HttpClient client = new HttpClient();
            HttpContent content;
            if (_contentType != null)
                content = new StringContent(postData, Encoding.UTF8, "application/json");
            else
                content = new StringContent(postData);
            HttpResponse httpResponse = new HttpResponse();
            try
            {
                HttpResponseMessage response = await client.PostAsync(_requestUrl, content);
                if (response.IsSuccessStatusCode)
                {
                    httpResponse.HttpStatusCode = response.StatusCode;
                    httpResponse.Result = await response.Content.ReadAsStringAsync();
                }
            }
            catch(Exception e)
            {
                httpResponse.Error = e;
            }
            
            if(_responseAction != null)
            {
                _responseAction(httpResponse);
            }
        }

        private void CompleteRequest(IAsyncResult httpRequest)
        {
            HttpResponse httpResponse = new HttpResponse();
            try
            {
                HttpWebRequest request = (HttpWebRequest)httpRequest.AsyncState;
                HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(httpRequest);
                httpResponse.HttpStatusCode = response.StatusCode;
                StreamReader resultReader = new StreamReader(response.GetResponseStream());
                httpResponse.Result = resultReader.ReadToEnd();
            }
            catch(Exception e)
            {
                httpResponse.Error = e;
            }

            if(_responseAction != null)
            {
                _responseAction(httpResponse);
            }
        }
    }
}
