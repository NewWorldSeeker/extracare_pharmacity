﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ExtraCarePharmacity.DataAccess.Helpers
{
    public class HttpResponse
    {
        public Exception Error { get; set; }
        public HttpStatusCode HttpStatusCode { get; set; }
        public string Result { get; set; }
    }
}
