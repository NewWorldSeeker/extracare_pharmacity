﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExtraCarePharmacity.DataAccess.ServiceModels;

namespace ExtraCarePharmacity.DataAccess.Interfaces
{
    public interface IServiceAccess
    {
        Task<LoginResult> Login(string email);
        Task<bool> SignUp(SignUpPostData signUpPostData);
        Task<BalanceResult> GetBalance(string sessionKey);
        Task<InboxResult> GetInboxes(string sessionKey);
        Task<SignOthersOutResult> SignOthersOut(string sessionKey);
        Task<SignOutResult> SignOut(string sessionKey);
        Task<StoreResult> GetStores();
        Task<TransactionResult> GetTransactions(string sessionKey);
        Task<TransactionDetailResult> GetTransactionDetails(string sessionKey, string transactionId);
        Task<ReminderResult> GetReminders(string sessionKey);
    }
}
