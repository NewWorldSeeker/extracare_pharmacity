﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using ExtraCarePharmacity.DataAccess.Helpers;
using ExtraCarePharmacity.DataAccess.Interfaces;
using ExtraCarePharmacity.DataAccess.ServiceModels;
using Kaliko;
using Newtonsoft.Json;

namespace ExtraCarePharmacity.DataAccess
{
    public class ServiceAccess: IServiceAccess
    {
        private static readonly string LoginUrl = "http://app.pharmacity.vn/ec2/api/Login";
        private static readonly string BalanceUrl = "http://app.pharmacity.vn/ec2/api/Balance";
        private static readonly string TransactionsUrl = "http://app.pharmacity.vn/ec2/api/Transactions";
        private static readonly string TransactionDetailsUrl = "http://app.pharmacity.vn/ec2/api/TransactionDetails";
        private static readonly string StoresUrl = "http://app.pharmacity.vn/ec2/api/Stores";
        private static readonly string SignOutUrl = "http://app.pharmacity.vn/ec2/api/SignOut";
        private static readonly string SignOthersOutUrl = "http://app.pharmacity.vn/ec2/api/SignOthersOut";
        private static readonly string InboxsUrl = "http://app.pharmacity.vn/ec2/api/Inbox";
        private static readonly string SignUpUrl = "http://app.pharmacity.vn/ec2/api/SignUp";

        #region IServiceAccess Members

        public System.Threading.Tasks.Task<ServiceModels.LoginResult> Login(string email)
        {
            TaskCompletionSource<LoginResult> loginResultTask = new TaskCompletionSource<LoginResult>();
            HttpRequest request = new HttpRequest(LoginUrl, (response) =>
            {
                if (response.Error == null && response.HttpStatusCode == HttpStatusCode.OK)
                {
                    LoginResult result = JsonConvert.DeserializeObject<LoginResult>(response.Result);
                    loginResultTask.SetResult(result);
                }
                else
                {
                    loginResultTask.SetResult(null);
                    Debug.WriteLine("ServiceAccess.Login(...): Unable to get result.");
                }
            });
            request.DoGetRequest("email=" + email);

            return loginResultTask.Task;
        }

        public System.Threading.Tasks.Task<ServiceModels.BalanceResult> GetBalance(string sessionKey)
        {
            TaskCompletionSource<BalanceResult> balanceResultTask = new TaskCompletionSource<BalanceResult>();
            HttpRequest request = new HttpRequest(BalanceUrl, (response) =>
            {
                if (response.Error == null && response.HttpStatusCode == HttpStatusCode.OK)
                {
                    BalanceResult result = JsonConvert.DeserializeObject<BalanceResult>(response.Result);
                    balanceResultTask.SetResult(result);
                }
                else
                {
                    balanceResultTask.SetResult(null);
                    Debug.WriteLine("ServiceAccess.GetBalance(...): Unable to get result.");
                }
            });
            request.DoGetRequest("session=" + sessionKey);

            return balanceResultTask.Task;
        }

        public  System.Threading.Tasks.Task<ServiceModels.InboxResult> GetInboxes(string sessionKey)
        {
            TaskCompletionSource<InboxResult> inboxResultTask = new TaskCompletionSource<InboxResult>();
            HttpRequest request = new HttpRequest(InboxsUrl, (response) =>
            {
                if (response.Error == null && response.HttpStatusCode == HttpStatusCode.OK)
                {
                    InboxResult result = JsonConvert.DeserializeObject<InboxResult>(response.Result);
                    inboxResultTask.SetResult(result);
                }
                else
                {
                    inboxResultTask.SetResult(null);
                    Debug.WriteLine("ServiceAccess.GetInboxs(...): Unable to get result.");
                }
            });
            request.DoGetRequest("session=" + sessionKey);

            return inboxResultTask.Task;
        }

        public System.Threading.Tasks.Task<ServiceModels.SignOthersOutResult> SignOthersOut(string sessionKey)
        {
            TaskCompletionSource<SignOthersOutResult> resultTask = new TaskCompletionSource<SignOthersOutResult>();
            HttpRequest request = new HttpRequest(SignOthersOutUrl, (response) =>
            {
                if (response.Error == null && response.HttpStatusCode == HttpStatusCode.OK)
                {
                    SignOthersOutResult result = JsonConvert.DeserializeObject<SignOthersOutResult>(response.Result);
                    resultTask.SetResult(result);
                }
                else
                {
                    resultTask.SetResult(null);
                    Debug.WriteLine("ServiceAccess.SignOthersOut(...): Unable to get result.");
                }
            });
            request.DoGetRequest("session=" + sessionKey);

            return resultTask.Task;
        }

        public System.Threading.Tasks.Task<ServiceModels.SignOutResult> SignOut(string sessionKey)
        {
            TaskCompletionSource<SignOutResult> resultTask = new TaskCompletionSource<SignOutResult>();
            HttpRequest request = new HttpRequest(SignOutUrl, (response) =>
            {
                if (response.Error == null && response.HttpStatusCode == HttpStatusCode.OK)
                {
                    SignOutResult result = JsonConvert.DeserializeObject<SignOutResult>(response.Result);
                    resultTask.SetResult(result);
                }
                else
                {
                    resultTask.SetResult(null);
                    Debug.WriteLine("ServiceAccess.SignOut(...): Unable to get result.");
                }
            });
            request.DoGetRequest("session=" + sessionKey);

            return resultTask.Task;
        }

        public System.Threading.Tasks.Task<ServiceModels.StoreResult> GetStores()
        {
            TaskCompletionSource<StoreResult> resultTask = new TaskCompletionSource<StoreResult>();
            HttpRequest request = new HttpRequest(StoresUrl, (response) =>
            {
                if (response.Error == null && response.HttpStatusCode == HttpStatusCode.OK)
                {
                    StoreResult result = JsonConvert.DeserializeObject<StoreResult>(response.Result);
                    resultTask.SetResult(result);
                }
                else
                {
                    resultTask.SetResult(null);
                    Debug.WriteLine("ServiceAccess.GetStores(...): Unable to get result.");
                }
            });
            request.DoGetRequest("");

            return resultTask.Task;
        }

        public System.Threading.Tasks.Task<ServiceModels.TransactionResult> GetTransactions(string sessionKey)
        {
            TaskCompletionSource<TransactionResult> resultTask = new TaskCompletionSource<TransactionResult>();
            HttpRequest request = new HttpRequest(TransactionsUrl, (response) =>
            {
                if (response.Error == null && response.HttpStatusCode == HttpStatusCode.OK)
                {
                    TransactionResult result = JsonConvert.DeserializeObject<TransactionResult>(response.Result);
                    resultTask.SetResult(result);
                }
                else
                {
                    resultTask.SetResult(null);
                    Debug.WriteLine("ServiceAccess.GetTransactions(...): Unable to get result.");
                }
            });
            request.DoGetRequest("session=" + sessionKey);

            return resultTask.Task;
        }

        #endregion

        #region IServiceAccess Members


        public Task<TransactionDetailResult> GetTransactionDetails(string sessionKey, string transactionId)
        {
            TaskCompletionSource<TransactionDetailResult> resultTask = new TaskCompletionSource<TransactionDetailResult>();
            HttpRequest request = new HttpRequest(TransactionDetailsUrl, (response) =>
            {
                if (response.Error == null && response.HttpStatusCode == HttpStatusCode.OK)
                {
                    TransactionDetailResult result = JsonConvert.DeserializeObject<TransactionDetailResult>(response.Result);
                    resultTask.SetResult(result);
                }
                else
                {
                    resultTask.SetResult(null);
                    Debug.WriteLine("ServiceAccess.GetTransactions(...): Unable to get result.");
                }
            });
            request.DoGetRequest("session=" + sessionKey + "&id=" + transactionId);

            return resultTask.Task;
        }

        public Task<ReminderResult> GetReminders(string sessionKey)
        {
            TaskCompletionSource<ReminderResult> resultTask = new TaskCompletionSource<ReminderResult>();
            HttpRequest request = new HttpRequest(TransactionDetailsUrl, (response) =>
            {
                if (response.Error == null && response.HttpStatusCode == HttpStatusCode.OK)
                {
                    ReminderResult result = JsonConvert.DeserializeObject<ReminderResult>(response.Result);
                    resultTask.SetResult(result);
                }
                else
                {
                    resultTask.SetResult(null);
                    Debug.WriteLine("ServiceAccess.GetReminders(...): Unable to get result.");
                }
            });
            request.DoGetRequest("session=" + sessionKey);

            return resultTask.Task;
        }

        #endregion




        public Task<bool> SignUp(SignUpPostData signUpPostData)
        {
            TaskCompletionSource<bool> resultTask = new TaskCompletionSource<bool>();
            HttpRequest request = new HttpRequest(SignUpUrl, (response) =>
            {
                if (response.Error == null && response.HttpStatusCode == HttpStatusCode.OK)
                {
                    SignUpResult result = JsonConvert.DeserializeObject<SignUpResult>(response.Result);
                    if(!string.IsNullOrEmpty(result.Email))
                    {
                        resultTask.SetResult(true);
                    }
                    else
                    {
                        resultTask.SetResult(false);
                    }
                }
                else
                {
                    resultTask.SetResult(false);
                    Debug.WriteLine("ServiceAccess.SignUp(...): Unable to get result.");
                }
            });
            request.SetContentType("application/json");
            request.DoPostRequest(JsonConvert.SerializeObject(signUpPostData));

            return resultTask.Task;
        }
    }
}
