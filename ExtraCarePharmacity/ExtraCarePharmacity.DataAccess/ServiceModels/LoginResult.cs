﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExtraCarePharmacity.DataAccess.ServiceModels
{
    public class LoginResult
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string FullName { get; set; }
        public string Session { get; set; }
        public string DefaultSession { get; set; }
        public string OTP { get; set; }
        public string CardNumber { get; set; }
        public bool Existed { get; set; }
        public bool isConfirmed { get; set; }
        public bool isValid { get; set; }
        public bool signOut { get; set; }
        public string LastConnected { get; set; }
    }
}
