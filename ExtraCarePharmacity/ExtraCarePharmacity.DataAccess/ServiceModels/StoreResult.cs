﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExtraCarePharmacity.DataAccess.ServiceModels
{
    public class StoreResult: List<StoreResultDetail>
    {

    }
    public class StoreResultDetail
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public string OpenTime { get; set; }
        public string CloseTime { get; set; }
        public string IsFMCG { get; set; }
    }
}
