﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtraCarePharmacity.DataAccess.ServiceModels
{
    public class ReminderResult
    {
        public bool IsValid
        { get; set; }
        public List<ReminderResultDetail> Reminder
        { get; set; }
    }

    public class ReminderResultDetail
    {
        public string Id
        { get; set; }
        public string Name
        { get; set; }
        public string Address
        { get; set; }
        public string Latitude
        { get; set; }
        public string Longitude
        { get; set; }
        public string OpenTime
        { get; set; }
        public string IsFMCG
        { get; set; }
    }
}
