﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtraCarePharmacity.DataAccess.ServiceModels
{
    public class TransactionDetailResultDetail
    {
        public string TransactionId { get; set; }
        public string ReceiptId { get; set; }
        public string ItemId { get; set; }
        public string Price { get; set; }
        public string TransDate { get; set; }
        public string TransTime { get; set; }
        public string Store { get; set; }
        public string Counter { get; set; }
        public string Unit { get; set; }
        public string StaffId { get; set; }
        public string SearchName { get; set; }
        public string FirstName { get; set; }
    }

    public class TransactionDetailResult
    {
        public bool isConfirmed { get; set; }
        public bool isValid { get; set; }
        public List<TransactionDetailResultDetail> List { get; set; }
    }
}
