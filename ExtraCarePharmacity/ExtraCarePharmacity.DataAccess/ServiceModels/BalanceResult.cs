﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExtraCarePharmacity.DataAccess.ServiceModels
{
    public class BalanceResult
    {
        public bool isConfirmed { get; set; }
        public bool isValid { get; set; }
        public Money Money { get; set; }
    }

    public class Money
    {
        public string Id { get; set; }
        public string Type { get; set; }
        public string RecentDate { get; set; }
        public string Cumulative { get; set; }
        public string Deal { get; set; }
    }
}
