﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExtraCarePharmacity.DataAccess.ServiceModels
{
    public class SignOthersOutResult
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string FullName { get; set; }
        public string Session { get; set; }
        public string DefaultSession { get; set; }
        public string OTP { get; set; }
        public string CardNumber { get; set; }
        public string Existed { get; set; }
        public string isConfirmed { get; set; }
        public string isValid { get; set; }
        public string signOut { get; set; }
        public string LastConnected { get; set; }
    }
}
