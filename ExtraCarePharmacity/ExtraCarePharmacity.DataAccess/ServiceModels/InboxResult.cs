﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExtraCarePharmacity.DataAccess.ServiceModels
{
    public class InboxResult : List<InboxResultDetail>
    { }
    public class InboxResultDetail
    {
        public string UserId { get; set; }
        public string HTML { get; set; }
        public string DateSendInbox { get; set; }
        public string ImgURL { get; set; }
    }
}
