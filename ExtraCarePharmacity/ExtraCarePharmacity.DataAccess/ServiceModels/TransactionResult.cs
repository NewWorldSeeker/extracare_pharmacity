﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace ExtraCarePharmacity.DataAccess.ServiceModels
{
    public class TransactionResultDetail
    {
        public string CardNumber { get; set; }
        public string ReceiptId { get; set; }
        public string TransactionId { get; set; }
        public string Loyaltytier { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string Money { get; set; }
    }
    
    public class TransactionResult
    {
        public bool isConfirmed { get; set; }
        public bool isValid { get; set; }
        public List<TransactionResultDetail> List { get; set; }
    }
}
