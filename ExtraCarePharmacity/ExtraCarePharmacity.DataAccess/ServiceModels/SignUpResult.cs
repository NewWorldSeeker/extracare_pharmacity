﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtraCarePharmacity.DataAccess.ServiceModels
{
    public class SignUpResult
    {
        public string CustomerName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string CustomerGroup { get; set; }
        public object Currency { get; set; }
        public string CardNumber { get; set; }
        public string Language { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public object Country { get; set; }
        public string StoreNumber { get; set; }
    }
}
