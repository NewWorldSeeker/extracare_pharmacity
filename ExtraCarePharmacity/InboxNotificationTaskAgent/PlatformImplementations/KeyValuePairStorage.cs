﻿using ExtraCarePharmacity.BusinessLogic.Interfaces;
using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InboxNotificationTaskAgent.PlatformImplementations
{
    class KeyValuePairStorage : IKeyValuePairStorage
    {
        //Dat's note: Since the data that need to cache is small. Application settings is
        //enough to store cache data. Because the design of this program applies "Program
        //to interface, not to implementation" principle. It is easy to change how 
        //KeyvaluePairStorage store data without affecting the rest of the application.
        private IsolatedStorageSettings _storage = IsolatedStorageSettings.ApplicationSettings;

        public KeyValuePairStorage()
        { }



        #region IKeyValuePairStorage Members

        public bool StoreValue(string key, string value)
        {
            _storage[key] = value;
            _storage.Save();
            return true;
        }

        public string LoadValue(string key)
        {
            //if (key == "SessionInfo")
            //    return "{\"Id\":11114,\"Email\":\"trangnguyen@pharmacity.vn\",\"PhoneNumber\":\"0909449084\",\"FullName\":\"Nguyen Thu Trang\",\"Session\":\"Zhho7z33aEGECajLyllRw\",\"DefaultSession\":\"Zhho7z33aEGECajLyllRw\",\"OTP\":\"\",\"CardNumber\":\"9992014000514\",\"Existed\":true,\"isConfirmed\":false,\"isValid\":true,\"signOut\":false,\"LastConnected\":\"2015-07-12T21:41:07.4225447+07:00\"}";

            string value;
            if (_storage.TryGetValue(key, out value))
            {
                return value;
            }
            else
            {
                return null;
            }
        }

        public bool Remove(string key)
        {
            bool removeResult = _storage.Remove(key);
            _storage.Save();
            return removeResult;
        }

        #endregion
    }
}
