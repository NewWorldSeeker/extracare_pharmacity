﻿using System.Diagnostics;
using System.Windows;
using Microsoft.Phone.Scheduler;
using System.Net.Http;
using System.Collections.Generic;
using ExtraCarePharmacity.BusinessLogic.BusinessModels;
using ExtraCarePharmacity.BusinessLogic.Interfaces;
using ExtraCarePharmacity.BusinessLogic;
using InboxNotificationTaskAgent.PlatformImplementations;
using ExtraCarePharmacity.Common.Helpers;
using Microsoft.Phone.Shell;

namespace InboxNotificationTaskAgent
{
    public class ScheduledAgent : ScheduledTaskAgent
    {
        /// <remarks>
        /// ScheduledAgent constructor, initializes the UnhandledException handler
        /// </remarks>
        static ScheduledAgent()
        {
            // Subscribe to the managed exception handler
            Deployment.Current.Dispatcher.BeginInvoke(delegate
            {
                Application.Current.UnhandledException += UnhandledException;
            });
        }

        /// Code to execute on Unhandled Exceptions
        private static void UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            if (Debugger.IsAttached)
            {
                // An unhandled exception has occurred; break into the debugger
                Debugger.Break();
            }
        }

        /// <summary>
        /// Agent that runs a scheduled task
        /// </summary>
        /// <param name="task">
        /// The invoked task
        /// </param>
        /// <remarks>
        /// This method is called when a periodic or resource intensive task is invoked
        /// </remarks>
        protected override void OnInvoke(ScheduledTask task)
        {
            //TODO: Add code to perform your task in background
            ICoreService coreService = new CoreService(new KeyValuePairStorage());
            List<Inbox> inboxes = Cloner.Clone(coreService.Inboxes);
            coreService.DataUpdated += (sender, args) =>
                {
                    if (args.DataPropertyName == "Inboxes")
                    {
                        List<Inbox> newInboxes = coreService.Inboxes;
                        if (newInboxes != null && newInboxes.Count > 0)
                        {
                            if (inboxes == null || inboxes.Count == 0)
                            {
                                CreateNewInboxToast();
                            }
                            else
                            {
                                if (newInboxes.Count > inboxes.Count)
                                {
                                    CreateNewInboxToast();
                                }
                            }
                        }
                    }
                };
            coreService.UpdateInboxes();

            NotifyComplete();
        }

        void CreateNewInboxToast()
        {
            ShellToast toast = new ShellToast();
            toast.Title = "ExtraCare Pharmacity";
            toast.Content = "Bạn có thư mới!";
            toast.NavigationUri = new System.Uri("/Views/InboxPage.xaml", System.UriKind.Relative);
        }
    }
}