﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using ExtraCarePharmacity.BusinessLogic.BusinessModels;
using ExtraCarePharmacity.Presentation.BaseTypes;
using ExtraCarePharmacity.Presentation.ViewModels;
using ExtraCarePharmacity.WP.PlatformImplementations;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace ExtraCarePharmacity.WP.Views
{
    public partial class TransactionsPage : PhoneApplicationPage
    {
        public ViewModelBase ViewModel
        { get; set; }

        public TransactionsPage()
        {
            ViewModel = new TransactionsPageViewModel(new KeyValuePairStorage(), new DispatcherAdapter(Dispatcher));
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            ViewModel.SetPageNavigator(new PageNavigator(this.NavigationService));

            Dictionary<string, string> navigationParams = new Dictionary<string, string>();
            foreach (KeyValuePair<string, string> item in NavigationContext.QueryString)
            {
                navigationParams.Add(item.Key, item.Value);
            }
            ViewModel.OnNavigatedTo(navigationParams);


            base.OnNavigatedTo(e);
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                Transaction transaction = (Transaction)e.AddedItems[0];
                (ViewModel as TransactionsPageViewModel).TransactionSelectedCommand.Execute(transaction);

            }
        }
    }
}