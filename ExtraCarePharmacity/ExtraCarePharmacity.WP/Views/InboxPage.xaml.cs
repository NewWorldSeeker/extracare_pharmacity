﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Navigation;
using ExtraCarePharmacity.Presentation.BaseTypes;
using ExtraCarePharmacity.Presentation.ViewModels;
using ExtraCarePharmacity.WP.PlatformImplementations;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace ExtraCarePharmacity.WP.Views
{
    public partial class InboxPage : PhoneApplicationPage, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public ViewModelBase ViewModel
        { get; set; }

        ImageSource _imageSource;
        public ImageSource ImageSource
        {
            get { return _imageSource; }
            set
            {
                if(!object.Equals(_imageSource, value))
                {
                    _imageSource = value;
                    if (PropertyChanged != null)
                        PropertyChanged(this, new PropertyChangedEventArgs("ImageSource"));
                }
            }
        }

        public InboxPage()
        {
            this.ViewModel = new InboxPageViewModel(new KeyValuePairStorage(), new DispatcherAdapter(Dispatcher));
            InitializeComponent();

            ViewModel.PropertyChanged += async (sender, args) =>
                {
                    if (args.PropertyName == "Inbox")
                    {
                        if ((ViewModel as InboxPageViewModel).Inbox != null)
                        {
                            ImageSource = await ImageCache.Instance.GetImage((ViewModel as InboxPageViewModel).Inbox.ImageUrl);
                            _webBrowser.NavigateToString("<html><head><meta name='viewport' content='user-scalable=no'/></head><body>"+ (ViewModel as InboxPageViewModel).Inbox.HtmlData + "</body></html>");
                        }
                    }
                };
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            ViewModel.SetPageNavigator(new PageNavigator(this.NavigationService));

            Dictionary<string, string> navigationParams = new Dictionary<string, string>();
            foreach(KeyValuePair<string, string> item in NavigationContext.QueryString)
            {
                navigationParams.Add(item.Key, item.Value);
            }
            ViewModel.OnNavigatedTo(navigationParams);


            base.OnNavigatedTo(e);
        }
    }
}