﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using ExtraCarePharmacity.Presentation.BaseTypes;
using ExtraCarePharmacity.Presentation.ViewModels;
using ExtraCarePharmacity.WP.PlatformImplementations;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace ExtraCarePharmacity.WP.Views
{
    public partial class SignUpPage : PhoneApplicationPage
    {
        public ViewModelBase ViewModel
        { get; set; }

        public SignUpPage()
        {
            this.ViewModel = new SignUpPageViewModel(new KeyValuePairStorage(), new DispatcherAdapter(Dispatcher));
            InitializeComponent();

            (ViewModel as SignUpPageViewModel).ShowMessage += (sender, args) =>
                {
                    MessageBox.Show(args.EventData);
                };
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            ViewModel.SetPageNavigator(new PageNavigator(this.NavigationService));

            Dictionary<string, string> navigationParams = new Dictionary<string, string>();
            foreach(KeyValuePair<string, string> item in NavigationContext.QueryString)
            {
                navigationParams.Add(item.Key, item.Value);
            }
            ViewModel.OnNavigatedTo(navigationParams);


            base.OnNavigatedTo(e);
        }

        private void FlatButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            //MessageBox.Show("Ứng dụng đang bảo trì, tính năng sẽ được đưa vào bản release chính thức sau 2 ngày nữa.");
        }
    }
}