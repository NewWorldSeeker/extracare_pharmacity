﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using ExtraCarePharmacity.Presentation.BaseTypes;
using ExtraCarePharmacity.WP.PlatformImplementations;
using ExtraCarePharmacity.Presentation.ViewModels;
using System.ComponentModel;
using Microsoft.Phone.Scheduler;

namespace ExtraCarePharmacity.WP.Views
{
    public partial class SettingsPage : PhoneApplicationPage, INotifyPropertyChanged
    {
        public ViewModelBase ViewModel
        { get; set; }

        const string InboxNotificationTaskAgentName = "InboxNotificationTaskAgent";

        public event PropertyChangedEventHandler PropertyChanged;

        bool _isNotificationEnabled = false;
        public bool IsNotificationEnabled
        {
            get { return _isNotificationEnabled; }
            set
            {
                if(!object.Equals(_isNotificationEnabled, value))
                {
                    _isNotificationEnabled = value;
                    if(PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("IsNotificationEnabled"));
                    }
                }
            }
        }

        public SettingsPage()
        {
            this.ViewModel = new SettingsPageViewModel(new KeyValuePairStorage(), new DispatcherAdapter(Dispatcher));
            InitializeComponent();

            //Check background task agents:
            PeriodicTask taskAgent = ScheduledActionService.Find(InboxNotificationTaskAgentName) as PeriodicTask;
            if(taskAgent != null)
            {
                IsNotificationEnabled = true;
            }
            else
            {
                IsNotificationEnabled = false;
            }

            this.PropertyChanged += (sender, args) =>
                {
                    if (args.PropertyName == "IsNotificationEnabled")
                    {
                        PeriodicTask task = ScheduledActionService.Find(InboxNotificationTaskAgentName) as PeriodicTask;
                        if (IsNotificationEnabled == true)
                        {
                            if (task == null)
                            {
                                task = new PeriodicTask(InboxNotificationTaskAgentName);
                                task.Description = "Background task that update inboxes and do toast notification.";
                                ScheduledActionService.Add(task);
                            }
                        }
                        else
                        {
                            ScheduledActionService.Remove(InboxNotificationTaskAgentName);
                        }
                    }
                };
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            ViewModel.SetPageNavigator(new PageNavigator(this.NavigationService));

            Dictionary<string, string> navigationParams = new Dictionary<string, string>();
            foreach(KeyValuePair<string, string> item in NavigationContext.QueryString)
            {
                navigationParams.Add(item.Key, item.Value);
            }
            ViewModel.OnNavigatedTo(navigationParams);


            base.OnNavigatedTo(e);
        }
    }
}