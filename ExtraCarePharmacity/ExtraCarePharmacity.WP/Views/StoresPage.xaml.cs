﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Device.Location;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using ExtraCarePharmacity.Common.Helpers;
using ExtraCarePharmacity.Presentation.BaseTypes;
using ExtraCarePharmacity.Presentation.BindingModels;
using ExtraCarePharmacity.Presentation.ViewModels;
using ExtraCarePharmacity.WP.PlatformImplementations;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Maps.Controls;
using Microsoft.Phone.Maps.Services;
using Microsoft.Phone.Maps.Toolkit;
using Microsoft.Phone.Shell;

namespace ExtraCarePharmacity.WP.Views
{
    public partial class StoresPage : PhoneApplicationPage
    {
        RouteQuery _routeQuery = new RouteQuery();
        MapRoute _previousRoute = null;

        public ViewModelBase ViewModel
        { get; set; }

        public StoresPage()
        {
            this.ViewModel = new StoresPageViewModel(new KeyValuePairStorage(), new DispatcherAdapter(Dispatcher), new GeoLocatorAdapter());
            InitializeComponent();

            this.ViewModel.PropertyChanged += (sender, args) =>
                {
                    StoresPageViewModel viewModel = ViewModel as StoresPageViewModel;

                    if (args.PropertyName == "Stores")
                    {
                        ObservableCollection<UIStore> stores = viewModel.Stores;
                        if (stores == null)
                            return;

                        ObservableCollection<DependencyObject> mapChildren = MapExtensions.GetChildren(_storesMap);
                        MapItemsControl mapItemsControl = mapChildren.FirstOrDefault(x => x is MapItemsControl) as MapItemsControl;
                        if(mapItemsControl.ItemsSource == null)
                        {
                            mapItemsControl.ItemsSource = stores;
                        }
                        else
                        {
                            Assign(mapItemsControl.ItemsSource as ObservableCollection<UIStore>, stores);
                        }
                    }
                    else if(args.PropertyName == "MapViewLocation")
                    {
                        _storesMap.SetView(new GeoCoordinate(viewModel.MapViewLocation.Latitude, viewModel.MapViewLocation.Longitude), 15);
                    }
                    else if(args.PropertyName == "UserCoordinate")
                    {
                        ObservableCollection<DependencyObject> mapChildren = MapExtensions.GetChildren(_storesMap);
                        Pushpin userMarker = mapChildren.FirstOrDefault(x => x is Pushpin) as Pushpin;
                        userMarker.GeoCoordinate = new GeoCoordinate(viewModel.UserCoordinate.Latitude, viewModel.UserCoordinate.Longitude);
                    }
                };


            (ViewModel as StoresPageViewModel).StoreLoadingFailed += (sender, args) =>
                {
                    MessageBox.Show("Không thể load địa điểm các nhà thuốc, vui lòng kiểm tra lại kết nối mạng.");
                };
            (ViewModel as StoresPageViewModel).LocationGettingFailed += (sender, args) =>
            {
                MessageBox.Show("Không thể lấy địa chỉ hiện tại, vui lòng bật location (GPS) trong settings của điện thoại.");
            };

            _routeQuery.QueryCompleted += routeQuery_QueryCompleted;
        }


        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            ViewModel.SetPageNavigator(new PageNavigator(this.NavigationService));

            Dictionary<string, string> navigationParams = new Dictionary<string, string>();
            foreach(KeyValuePair<string, string> item in NavigationContext.QueryString)
            {
                navigationParams.Add(item.Key, item.Value);
            }
            ViewModel.OnNavigatedTo(navigationParams);
            
            if(! await InternetConnection.CheckAvailability())
            {
                MessageBox.Show("Không thể kết nối đến server, vui lòng kiểm tra lại kết nối mạng!");
            }

            base.OnNavigatedTo(e);
        }

        static void Assign(ObservableCollection<UIStore> stores, IList<UIStore> source)
        {
            stores.Clear();
            foreach(var item in source)
            {
                stores.Add(item);
            }
        }

        private void PushpinGrid_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            RemovePreviousRoute();

            string storeId = (string)((Image)e.OriginalSource).Tag;
            (ViewModel as StoresPageViewModel).StoreTapCommand.Execute(storeId);
        }

        private void CreateRoute(GeoCoordinate coordinate1, GeoCoordinate coordinate2)
        {
            if (!_routeQuery.IsBusy)
            {
                RemovePreviousRoute();

                List<GeoCoordinate> coordinates = new List<GeoCoordinate>();
                coordinates.Add(coordinate1);
                coordinates.Add(coordinate2);
                _routeQuery.Waypoints = coordinates;
                _routeQuery.QueryAsync();
            }
        }

        private void RemovePreviousRoute()
        {
            if (_previousRoute != null)
            {
                _storesMap.RemoveRoute(_previousRoute);
                _previousRoute = null;
            }
        }

        void routeQuery_QueryCompleted(object sender, QueryCompletedEventArgs<Route> e)
        {
            Route route = e.Result;
            MapRoute mapRoute = new MapRoute(route);
            _storesMap.AddRoute(mapRoute);
            _previousRoute = mapRoute;
        }

        private void ImageButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            LatitudeLongitude userLocation = (ViewModel as StoresPageViewModel).UserCoordinate;
            UIStore focusedStore = (ViewModel as StoresPageViewModel).FocusedStore;
            if(userLocation != null && focusedStore != null)
            {
                GeoCoordinate coordinate1 = new GeoCoordinate(userLocation.Latitude, userLocation.Longitude);
                GeoCoordinate coordinate2 = new GeoCoordinate(focusedStore.Latitude, focusedStore.Longitude);
                CreateRoute(coordinate1, coordinate2);
            }
        }

        private void _storesMap_Loaded(object sender, RoutedEventArgs e)
        {
            Microsoft.Phone.Maps.MapsSettings.ApplicationContext.ApplicationId = "9f6cea33-eb06-44e0-8ecf-9873f3446aad";
            Microsoft.Phone.Maps.MapsSettings.ApplicationContext.AuthenticationToken = "AzhPJSYF_cxVQe3ZUk-YnQ";
        }
    }
}