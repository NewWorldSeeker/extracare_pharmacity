﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using ExtraCarePharmacity.Presentation.BaseTypes;
using ExtraCarePharmacity.Presentation.ViewModels;
using ExtraCarePharmacity.WP.PlatformImplementations;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace ExtraCarePharmacity.WP.Views
{
    public partial class SignInPage : PhoneApplicationPage
    {
        public ViewModelBase ViewModel
        { get; set; }

        public SignInPage()
        {
            this.ViewModel = new SignInPageViewModel(new KeyValuePairStorage(), new DispatcherAdapter(Dispatcher));
            InitializeComponent();
            (ViewModel as SignInPageViewModel).SignInError += (sender, args) =>
                {
                    MessageBox.Show("Không thể đăng nhập. Vui lòng kiểm tra lại email/SĐT hoặc kết nối mạng.");
                };
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            ViewModel.SetPageNavigator(new PageNavigator(this.NavigationService));

            Dictionary<string, string> navigationParams = new Dictionary<string, string>();
            foreach(KeyValuePair<string, string> item in NavigationContext.QueryString)
            {
                navigationParams.Add(item.Key, item.Value);
            }
            ViewModel.OnNavigatedTo(navigationParams);


            base.OnNavigatedTo(e);
        }
    }
}