﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using ExtraCarePharmacity.Presentation.BaseTypes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using ExtraCarePharmacity.Presentation.ViewModels;
using ExtraCarePharmacity.WP.PlatformImplementations;

namespace ExtraCarePharmacity.WP.Views
{
    public partial class HomePage : PhoneApplicationPage
    {
        public ViewModelBase ViewModel
        { get; set; }

        public HomePage()
        {
            this.ViewModel = new HomePageViewModel(new KeyValuePairStorage(), new DispatcherAdapter(Dispatcher));
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            ViewModel.SetPageNavigator(new PageNavigator(this.NavigationService));

            Dictionary<string, string> navigationParams = new Dictionary<string, string>();
            foreach(KeyValuePair<string, string> item in NavigationContext.QueryString)
            {
                navigationParams.Add(item.Key, item.Value);
            }
            ViewModel.OnNavigatedTo(navigationParams);


            base.OnNavigatedTo(e);
        }
    }
}