﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using ExtraCarePharmacity.BusinessLogic;
using ExtraCarePharmacity.BusinessLogic.Interfaces;
using ExtraCarePharmacity.Presentation.BaseTypes;
using ExtraCarePharmacity.Presentation.ViewModels;
using ExtraCarePharmacity.WP.PlatformImplementations;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace ExtraCarePharmacity.WP.Views
{
    public partial class SplashScreenPage : PhoneApplicationPage
    {
        ICoreService _coreService;

        public ViewModelBase ViewModel
        { get; set; }

        public SplashScreenPage()
        {
            this.ViewModel = new HomePageViewModel(new KeyValuePairStorage(), new DispatcherAdapter(Dispatcher));
            InitializeComponent();

            _coreService = new CoreService(new KeyValuePairStorage());
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            ViewModel.SetPageNavigator(new PageNavigator(this.NavigationService));

            Dictionary<string, string> navigationParams = new Dictionary<string, string>();
            foreach(KeyValuePair<string, string> item in NavigationContext.QueryString)
            {
                navigationParams.Add(item.Key, item.Value);
            }
            ViewModel.OnNavigatedTo(navigationParams);


            base.OnNavigatedTo(e);
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            _logoZoomingStoryboard.Begin();
            _logoZoomingStoryboard.Completed += async (s, args) =>
                {
                    if (await _coreService.CheckIsLoggedIn())
                    {
                        NavigationService.Navigate(new Uri("/Views/HomePage.xaml", UriKind.Relative));
                    }
                    else
                    {
                        NavigationService.Navigate(new Uri("/Views/SignInPage.xaml", UriKind.Relative));
                    }
                };
        }
    }
}