﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using ExtraCarePharmacity.Presentation.BaseTypes;
using ExtraCarePharmacity.Presentation.ViewModels;
using ExtraCarePharmacity.WP.PlatformImplementations;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace ExtraCarePharmacity.WP.Views
{
    public partial class WebBrowserPage : PhoneApplicationPage
    {
        public ViewModelBase ViewModel
        { get; set; }

        public WebBrowserPage()
        {
            this.ViewModel = new WebBrowserPageViewModel(new KeyValuePairStorage(), new DispatcherAdapter(Dispatcher));
            InitializeComponent();
            wbBrowser.ClearCookiesAsync();

        }

        private void wbBrowser_LoadCompleted(object sender, NavigationEventArgs e)
        {
            progessbar.Visibility = Visibility.Collapsed;
            progessbar.IsIndeterminate = false;
        }

        private void wbBrowser_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            progessbar.Visibility = Visibility.Collapsed;
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            ViewModel.SetPageNavigator(new PageNavigator(this.NavigationService));

            Dictionary<string, string> navigationParams = new Dictionary<string, string>();
            foreach(KeyValuePair<string, string> item in NavigationContext.QueryString)
            {
                navigationParams.Add(item.Key, item.Value);
            }
            ViewModel.OnNavigatedTo(navigationParams);

            string link = "";
            string title = "";
            if (NavigationContext.QueryString.TryGetValue("title", out title) && NavigationContext.QueryString.TryGetValue("link", out link))
            {
                txtTitle.Text = title;
                wbBrowser.Source = new Uri(link, UriKind.Absolute);
            }      

            base.OnNavigatedTo(e);
        }

        //about us: http://nhathuoconline.vn/gioi-thieu.html
        // home: http://nhathuoconline.vn/
        // Terms: http://nhathuoconline.vn/thoa-thuan-quy-dinh.html
        // Policy: http://nhathuoconline.vn/dieu-khoan-su-dung.html
    }
}