﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Navigation;
using ExtraCarePharmacity.Presentation.BaseTypes;
using ExtraCarePharmacity.Presentation.ViewModels;
using ExtraCarePharmacity.WP.PlatformImplementations;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media.Imaging;
using System.IO;
using System.Windows.Threading;

namespace ExtraCarePharmacity.WP.Views
{
    public partial class QRCodePage : PhoneApplicationPage, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        DispatcherTimer _currentTimeUpdatingTimer;

        public ViewModelBase ViewModel
        { get; set; }

        BitmapImage _imageSource;
        public BitmapImage ImageSource
        {
            get { return _imageSource; }
            set
            {
                if(!object.Equals(_imageSource, value))
                {
                    _imageSource = value;
                    if(PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImageSource"));
                    }
                }
            }
        }

        DateTime _currentTime;
        public DateTime CurrentTime
        {
            get
            {
                return _currentTime;
            }
            set
            {
               if(!object.Equals(_currentTime, value))
               {
                   _currentTime = value;
                   if(PropertyChanged != null)
                   {
                       PropertyChanged(this, new PropertyChangedEventArgs("CurrentTime"));
                   }
               }
            }
        }

        public QRCodePage()
        {
            this.ViewModel = new QRCodePageViewModel(new KeyValuePairStorage(), new DispatcherAdapter(Dispatcher));
            InitializeComponent();
            _currentTimeUpdatingTimer = new DispatcherTimer();
            _currentTimeUpdatingTimer.Interval = new TimeSpan(0, 0, 1);
            _currentTimeUpdatingTimer.Tick += (sender, args) =>
                {
                    CurrentTime = DateTime.Now;
                };
            _currentTimeUpdatingTimer.Start();

            this.ViewModel.PropertyChanged += (sender, args) =>
                {
                    if (args.PropertyName == "CardNumber")
                    {
                        QRCodePageViewModel viewModel = ViewModel as QRCodePageViewModel;
                        if (!string.IsNullOrEmpty(viewModel.CardNumber))
                        {
                            ZXing.BarcodeWriter writer = new ZXing.BarcodeWriter();
                            writer.Format = ZXing.BarcodeFormat.QR_CODE;
                            ZXing.Common.BitMatrix bitMatrix = writer.Encode(viewModel.CardNumber);
                            WriteableBitmap bitmap = writer.Write(bitMatrix);
                            MemoryStream memoryStream = new MemoryStream();
                            bitmap.SaveJpeg(memoryStream, bitmap.PixelWidth, bitmap.PixelHeight, 0, 100);
                            BitmapImage bitmapImage = new BitmapImage();
                            bitmapImage.SetSource(memoryStream);
                            ImageSource = bitmapImage;
                        }
                    }
                };
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            ViewModel.SetPageNavigator(new PageNavigator(this.NavigationService));

            Dictionary<string, string> navigationParams = new Dictionary<string, string>();
            foreach(KeyValuePair<string, string> item in NavigationContext.QueryString)
            {
                navigationParams.Add(item.Key, item.Value);
            }
            ViewModel.OnNavigatedTo(navigationParams);


            base.OnNavigatedTo(e);
        }
    }
}