﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using ExtraCarePharmacity.BusinessLogic.BusinessModels;
using ExtraCarePharmacity.Common.Helpers;

namespace ExtraCarePharmacity.WP.Converters
{
    public class HomePageBalanceCumulativeConverter: IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if(value == null)
            {
                return "N/A";
            }

            Balance balance = (Balance)value;

            if (balance.Cumulative >= 1)
            {
                return StringFormat.FormatCurrency((int)balance.Cumulative, ".") + " đ";
            }
            else
            {
                return "N/A";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
