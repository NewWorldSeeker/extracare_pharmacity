﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Data;
using ExtraCarePharmacity.Presentation.BindingModels;

namespace ExtraCarePharmacity.WP.Converters
{
    class StoresPageUIStoreToOpenCloseTimeRemainingConverter: IValueConverter
    {

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            UIStore store = (UIStore)value;
            //Regex regex = new Regex(@"\d+:\d+\s+(AM|PM)");
            DateTime openingTime = DateTime.ParseExact(store.OpeningTime, "hh:mm tt", CultureInfo.InvariantCulture);
            DateTime closingTime = DateTime.ParseExact(store.ClosingTime, "hh:mm tt", CultureInfo.InvariantCulture);
            DateTime currentTime = new DateTime(1, 1, 1, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
