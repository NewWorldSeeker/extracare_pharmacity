﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using ExtraCarePharmacity.Presentation.BindingModels;

namespace ExtraCarePharmacity.WP.Converters
{
    public class StoresPageGeoCoordinateConverter: IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            UIStore store = (UIStore)value;
            GeoCoordinate coordinate = new GeoCoordinate(store.Latitude, store.Longitude);
            return coordinate;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
