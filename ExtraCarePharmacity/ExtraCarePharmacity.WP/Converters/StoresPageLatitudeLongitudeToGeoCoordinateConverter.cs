﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using ExtraCarePharmacity.Presentation.ViewModels;

namespace ExtraCarePharmacity.WP.Converters
{
    public class StoresPageLatitudeLongitudeToGeoCoordinateConverter: IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            LatitudeLongitude coordinate = (LatitudeLongitude)value;
            GeoCoordinate geoCoordinate = new GeoCoordinate(coordinate.Latitude, coordinate.Longitude);
            return geoCoordinate;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
