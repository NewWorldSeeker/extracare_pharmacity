﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using ExtraCarePharmacity.Common.Helpers;

namespace ExtraCarePharmacity.WP.Converters
{
    public class TransactionsPageCurrencyConverter: IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int currency = (int)((decimal)value);
            if(currency < 0)
            {
                string result = StringFormat.FormatCurrency(-currency, ".");
                return "-" + result + " đ";
            }
            else
            {
                string result = StringFormat.FormatCurrency(currency, ".");
                return "+" + result + " đ";
            }
            
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
