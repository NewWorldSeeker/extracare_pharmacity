﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using ExtraCarePharmacity.BusinessLogic.BusinessModels;
using ExtraCarePharmacity.Presentation.BindingModels;
using ExtraCarePharmacity.WP.UIModels;

namespace ExtraCarePharmacity.WP.Converters
{
    public class StoreCollectionToUIStoreCollectionConverter: IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            ObservableCollection<Store> storeCollection = (ObservableCollection<Store>)value;
            ObservableCollection<UIStore> uiStoreCollection = new ObservableCollection<UIStore>();
            foreach(var item in storeCollection)
            {
                uiStoreCollection.Add(new UIStore(item));
            }
            return uiStoreCollection;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
