﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExtraCarePharmacity.Presentation.Interfaces;
using Windows.Devices.Geolocation;

namespace ExtraCarePharmacity.WP.PlatformImplementations
{
    class GeoLocatorAdapter: IGeoLocator
    {
        Geolocator _geoLocator;

        public GeoLocatorAdapter()
        {
            _geoLocator = new Geolocator();
            _geoLocator.DesiredAccuracyInMeters = 50;
        }

        #region IGeoLocator Members

        public async Task<IGeoLocation> GetLocationAsync(TimeSpan maxCacheAge, TimeSpan timeOut)
        {
            IGeoLocation result = new GeoLocation();
            try
            {
                Geoposition position = await _geoLocator.GetGeopositionAsync(maxCacheAge, timeOut);
                result.Latitude = position.Coordinate.Latitude;
                result.Longitude = position.Coordinate.Longitude;
                return result;
            }
            catch(Exception e)
            {
                Debug.WriteLine(e.Message);
                return null;
            }
        }
        #endregion
    }
}
