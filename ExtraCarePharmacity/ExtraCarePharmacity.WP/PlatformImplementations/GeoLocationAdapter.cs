﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExtraCarePharmacity.Presentation.Interfaces;

namespace ExtraCarePharmacity.WP.PlatformImplementations
{
    public class GeoLocationAdapter: IGeoLocation
    {
        GeoCoordinate _coordinate;

        public GeoLocationAdapter(GeoCoordinate coordinate)
        {
            _coordinate = coordinate;
        }

        public double Latitude
        {
            get { return _coordinate.Latitude; }
            set { _coordinate.Latitude = value; }
        }

        public double Longitude
        {
            get { return _coordinate.Longitude; }
            set { _coordinate.Longitude = value; }
        }
    }
}
