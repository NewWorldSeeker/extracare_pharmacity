﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace ExtraCarePharmacity.WP.PlatformImplementations
{
    class ImageCache
    {
        #region Singleton pattern implementations
        static ImageCache s_instance;
        public static ImageCache Instance
        {
            get
            {
                if(s_instance == null)
                {
                    s_instance = new ImageCache();
                }

                return s_instance;
            }
        }
        private ImageCache()
        { }
        #endregion

        public async Task<BitmapImage> GetImage(string imageUrl)
        {
            string cacheFileName = imageUrl.Replace("http://", "").Replace("/", ".");
            BitmapImage result = null;

            using(IsolatedStorageFile storage = IsolatedStorageFile.GetUserStoreForApplication())
            {
                //Check whether cache existed:
                if (storage.FileExists(cacheFileName))
                {
                    using(IsolatedStorageFileStream fileStream = storage.OpenFile(cacheFileName, System.IO.FileMode.Open))
                    {
                        result = new BitmapImage();
                        result.SetSource(fileStream);
                    }
                }
                else
                {
                    HttpClient client = new HttpClient();
                    HttpResponseMessage response = await client.GetAsync(imageUrl);
                    if(response.IsSuccessStatusCode)
                    {
                        byte[] imageData = await response.Content.ReadAsByteArrayAsync();
                        using(IsolatedStorageFileStream fileStream = storage.CreateFile(cacheFileName))
                        {
                            MemoryStream imageStream = new MemoryStream();
                            for(int i = 0; i < imageData.Length; i++)
                            {
                                imageStream.WriteByte(imageData[i]);
                                fileStream.WriteByte(imageData[i]);
                            }
                            fileStream.Close();
                            result = new BitmapImage();
                            result.SetSource(imageStream);
                        }
                    }
                }

                return result;
            }
        }
    }
}
