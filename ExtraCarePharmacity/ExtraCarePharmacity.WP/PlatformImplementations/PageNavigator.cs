﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Navigation;
using ExtraCarePharmacity.Presentation.Interfaces;

namespace ExtraCarePharmacity.WP.PlatformImplementations
{
    class PageNavigator: IPageNavigator
    {
        NavigationService _navigationService;

        public PageNavigator(NavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        #region IPageNavigator Members

        public bool Navigate(string pageName, Dictionary<string, string> navigationParams)
        {
            StringBuilder queryParamBuilder = new StringBuilder();
            foreach(KeyValuePair<string, string> keyValue in navigationParams)
            {
                queryParamBuilder.Append(string.Format("{0}={1}&", keyValue.Key, keyValue.Value));
            }
            if(queryParamBuilder.Length > 0)
            {
                //Remove last '&':
                queryParamBuilder.Remove(queryParamBuilder.Length - 1, 1);
            }

            string navigationUrl = "/Views/" + pageName + ".xaml?" + queryParamBuilder.ToString();
            bool navigationResult = _navigationService.Navigate(new Uri(navigationUrl, UriKind.Relative));

            return navigationResult;
        }


        #endregion

        #region IPageNavigator Members


        public bool RemoveBackEntry()
        {
            if(_navigationService.CanGoBack)
            {
                _navigationService.RemoveBackEntry();
                return true;
            }
            else
            {
                return false;
            }
        }

        public void ClearBackEntries()
        {
            while(_navigationService.CanGoBack)
            {
                _navigationService.RemoveBackEntry();
            }
        }

        #endregion
    }
}
