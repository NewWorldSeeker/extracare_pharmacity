﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using ExtraCarePharmacity.Presentation.Interfaces;

namespace ExtraCarePharmacity.WP.PlatformImplementations
{
    class DispatcherAdapter: IDispatcher
    {
        Dispatcher _dispatcher;

        public DispatcherAdapter(Dispatcher dispatcher)
        {
            _dispatcher = dispatcher;
        }

        #region IDispatcher Members

        public void BeginInvoke(Action action)
        {
            _dispatcher.BeginInvoke(action);
        }

        #endregion
    }
}
