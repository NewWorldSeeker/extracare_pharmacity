﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExtraCarePharmacity.BusinessLogic.Interfaces
{
    public interface IKeyValuePairStorage
    {
        bool StoreValue(string key, string value);
        string LoadValue(string key);
        bool Remove(string key);
    }
}
