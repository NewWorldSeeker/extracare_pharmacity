﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExtraCarePharmacity.BusinessLogic.BusinessModels;

namespace ExtraCarePharmacity.BusinessLogic.Interfaces
{
    public interface ICoreService
    {
        event EventHandler<DataUpdatedEventArgs> DataUpdated;

        Task<bool> SignUp(SignUpData signUpData);
        Task<bool> Login(string email);
        void Logout();
        Task<bool> CheckIsLoggedIn();
        bool CheckIsConfirm();
        bool CheckIsConnectionFailed();

        Balance Balance { get; }
        List<Inbox> Inboxes { get; }
        SessionInfo SessionInfo { get; }
        List<Store> Stores { get; }
        List<Transaction> Transactions { get; }
        List<Reminder> Reminders { get; }
        Task<List<TransactionDetail>> GetTransactionDetails(string transactionId);

        void UpdateBalance();
        void UpdateInboxes();
        void UpdateStores();
        void UpdateTransactions();
        void UpdateReminders();
    }

    public class DataUpdatedEventArgs: EventArgs
    {
        public string DataPropertyName { get; private set; }

        public DataUpdatedEventArgs(string dataPropertyName)
        {
            this.DataPropertyName = dataPropertyName;
        }
    }
}
