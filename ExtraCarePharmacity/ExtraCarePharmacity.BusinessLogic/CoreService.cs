﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Kaliko;
using ExtraCarePharmacity.BusinessLogic.BusinessModels;
using ExtraCarePharmacity.BusinessLogic.Interfaces;
using ExtraCarePharmacity.DataAccess.Interfaces;
using ExtraCarePharmacity.DataAccess;
using ExtraCarePharmacity.DataAccess.ServiceModels;
using System.Diagnostics;
using ExtraCarePharmacity.BusinessLogic.Exceptions;

namespace ExtraCarePharmacity.BusinessLogic
{
    /// <summary>
    /// This class responsible for providing core service of the application.
    /// </summary>
    public class CoreService: ICoreService
    {
        private IKeyValuePairStorage _keyValuePairStorage;
        private IServiceAccess _serviceAccess;

        private Dictionary<string, object> _dataMap = new Dictionary<string, object>();

        private SessionInfo _sessionInfo = null;

        bool _isConfirm = false;
        bool _isConnectionFailed = false;

        public CoreService(IKeyValuePairStorage keyValuePairStorage)
        {
            _keyValuePairStorage = keyValuePairStorage;
            _serviceAccess = new ServiceAccess();
        }

        #region IDataManager Members

        public event EventHandler<DataUpdatedEventArgs> DataUpdated;

        public Balance Balance
        {
            get 
            {
                return GetData<Balance>("Balance");
            }
        }

        public List<Inbox> Inboxes
        {
            get
            {
                return GetData<List<Inbox>>("Inboxes");
            }
        }

        public SessionInfo SessionInfo
        {
            get 
            { 
                if(_sessionInfo == null)
                {
                    string sessionInfoJson = _keyValuePairStorage.LoadValue("SessionInfo");
                    if(sessionInfoJson != null)
                    {
                        _sessionInfo = JsonConvert.DeserializeObject<SessionInfo>(sessionInfoJson);
                    }
                }

                return _sessionInfo;
            }
        }

        public List<Store> Stores
        {
            get 
            {
                return GetData<List<Store>>("Stores");
            }
        }

        public List<Transaction> Transactions
        {
            get 
            {
                return GetData<List<Transaction>>("Transactions");
            }
        }

        #endregion

        #region Helpers
        private T GetData<T>(string typeName)
        {
            if (!_dataMap.ContainsKey(typeName) || _dataMap[typeName] == null)
            {
                string dataString = _keyValuePairStorage.LoadValue(typeName);
                if (dataString != null)
                {
                    T deserializedObject = (T)JsonConvert.DeserializeObject<T>(dataString);
                    _dataMap.Add(typeName, deserializedObject);
                    return deserializedObject;
                }
            }
            else
            {
                return (T)_dataMap[typeName];
            }

            return default(T);
        }

        private void UpdateData<T>(string typeName)
        {
            Task updateTask = Task.Run(async () =>
            {
                try
                {
                    if (SessionInfo != null && SessionInfo.Session != null)
                    {
                        T updatedDataModel = (T)await FactoryGetDataAsync(typeName);

                        if (updatedDataModel != null)
                        {
                            _dataMap[typeName] = updatedDataModel;
                            _keyValuePairStorage.StoreValue(typeName, JsonConvert.SerializeObject(updatedDataModel));
                        }
                        if (DataUpdated != null)
                        {
                            DataUpdated(this, new DataUpdatedEventArgs(typeName));
                        }
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine("CoreService." + typeName + ": There is something wrong when updating " + typeName + "n");
                    Debug.WriteLine(e.ToString());
                }
            });
        }

        private async Task<object> FactoryGetDataAsync(string typeName)
        {
            if (SessionInfo == null || SessionInfo.Session == null)
                throw new Exception("There is no session.");

            _isConnectionFailed = false;

            switch(typeName)
            {
                case "Balance":
                    BalanceResult balanceResult = await _serviceAccess.GetBalance(_sessionInfo.Session);
                    if(balanceResult == null)
                    {
                        _isConnectionFailed = true;
                        return null;
                    }
                    _isConfirm = balanceResult.isConfirmed;
                    return Balance.From(balanceResult);
                case "Inboxes":
                    InboxResult inboxResult = await _serviceAccess.GetInboxes(_sessionInfo.Session);
                    if(inboxResult == null)
                    {
                        _isConnectionFailed = true;
                        return null;
                    }
                    return Inbox.From(inboxResult);
                case "Stores":
                    StoreResult storeResult = await _serviceAccess.GetStores();
                    if(storeResult == null)
                    {
                        _isConnectionFailed = true;
                        return null;
                    }
                    return Store.From(storeResult);
                case "Transactions":
                    TransactionResult transactionResult = await _serviceAccess.GetTransactions(_sessionInfo.Session);
                    if(transactionResult == null)
                    {
                        _isConnectionFailed = true;
                        return null;
                    }
                    _isConfirm = transactionResult.isConfirmed;
                    return Transaction.From(transactionResult);
                case "Reminders":
                    ReminderResult reminderResult = await _serviceAccess.GetReminders(_sessionInfo.Session);
                    if(reminderResult == null)
                    {
                        _isConnectionFailed = true;
                        return null;
                    }
                    return Reminder.From(reminderResult);
            }
            return null;
        }
        #endregion

        #region ICoreService Members


        public async Task<bool> Login(string email)
        {
            try
            {
                LoginResult loginResult = await _serviceAccess.Login(email);
                SessionInfo sessionInfo = SessionInfo.From(loginResult);
                if(sessionInfo.Session != null)
                {
                    _keyValuePairStorage.StoreValue("SessionInfo", JsonConvert.SerializeObject(sessionInfo));
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch(Exception exception)
            {
                Debug.WriteLine("CoreService.Login(): Can't login.");
                return false;
            }
        }

        public void Logout()
        {
            foreach(string item in new string[]{"SessionInfo", "Balance", "Transactions"})
            {
                _keyValuePairStorage.Remove(item);
            }
        }

        public async Task<bool> CheckIsLoggedIn()
        {
            string sessionInfoJson = _keyValuePairStorage.LoadValue("SessionInfo");
            if (sessionInfoJson == null)
                return false;

            SessionInfo sessionInfo = JsonConvert.DeserializeObject<SessionInfo>(sessionInfoJson);
            if(sessionInfo.Session == null)
                return false;

            //I'm doing the abnormal way because the API is so ...
            //At least, there must be an service api to check whether the "session key" is expired or not.
            //But there isn't, so here is the abnormal code to check ... (In fact, there is, but it is mixed with other api)
            //At least, don't ask me why i'm doing this, this is not my fault, I'm not responsible for this.

            BalanceResult balanceResult = await _serviceAccess.GetBalance(sessionInfo.Session);
            if (balanceResult == null)
                return true;
            if (!balanceResult.isValid)
                return false;

            return true;
        }

        #endregion

        #region ICoreService Members


        public bool CheckIsConfirm()
        {
            return _isConfirm;
        }

        public async Task<List<TransactionDetail>> GetTransactionDetails(string transactionId)
        {
            if (SessionInfo == null || SessionInfo.Session == null)
                throw new Exception("There is no session.");

            TransactionDetailResult serviceResult = await _serviceAccess.GetTransactionDetails(_sessionInfo.Session, transactionId);
            List<TransactionDetail> result = TransactionDetail.From(serviceResult);
            _isConfirm = serviceResult.isConfirmed;

            return result;
        }

        public void UpdateBalance()
        {
            UpdateData<Balance>("Balance");
        }

        public void UpdateInboxes()
        {
            UpdateData<List<Inbox>>("Inboxes");
        }

        public void UpdateStores()
        {
            UpdateData<List<Store>>("Stores");
        }

        public void UpdateTransactions()
        {
            UpdateData<List<Transaction>>("Transactions");
        }

        #endregion

        #region ICoreService Members


        public bool CheckIsConnectionFailed()
        {
            return _isConnectionFailed;
        }

        #endregion


        public Task<bool> SignUp(SignUpData signUpData)
        {
            return _serviceAccess.SignUp(signUpData.ToPostData());
        }


        public List<Reminder> Reminders
        {
            get 
            {
                return GetData<List<Reminder>>("Reminders");
            }
        }

        public void UpdateReminders()
        {
            UpdateData<List<Reminder>>("Reminders");
        }
    }

}
