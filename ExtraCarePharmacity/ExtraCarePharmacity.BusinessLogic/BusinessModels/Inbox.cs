﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using ExtraCarePharmacity.DataAccess.ServiceModels;
using Kaliko;

namespace ExtraCarePharmacity.BusinessLogic.BusinessModels
{
    public class Inbox
    {
        public string UserId { get; set; }
        public DateTime DateSent { get; set; }
        public string HtmlData { get; set; }
        public string ImageUrl { get; set; }

        #region ADAPTING METHODS
        public static Inbox From(InboxResultDetail inboxResultDetail)
        {
            Inbox result = new Inbox();

            DateTime dateSent;
            if(!DateTime.TryParse(inboxResultDetail.DateSendInbox, out dateSent))
            {
                Debug.WriteLine("Inbox.From(...): Can't parse inboxResultDetail.DateSentInbox to DateTime.");
            }

            result.UserId = inboxResultDetail.UserId;
            result.DateSent = dateSent;
            result.HtmlData = inboxResultDetail.HTML;
            result.ImageUrl = inboxResultDetail.ImgURL;

            return result;
        }

        public static List<Inbox> From(InboxResult inboxResult)
        {
            List<Inbox> result = new List<Inbox>();

            foreach(InboxResultDetail item in inboxResult)
            {
                result.Add(Inbox.From(item));
            }

            return result;
        }
        #endregion
    }
}
