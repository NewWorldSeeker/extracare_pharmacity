﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using ExtraCarePharmacity.BusinessLogic.BusinessModels;
using ExtraCarePharmacity.DataAccess.ServiceModels;
using Kaliko;

namespace ExtraCarePharmacity.BusinessLogic.BusinessModels
{
    public class Balance
    {
        public string BalanceType { get; set; }
        public DateTime RecentlyRebalancedDate { get; set; }
        public decimal Cumulative { get; set; }
        public int NumOfDeals { get; set; }

        #region ADAPTING METHODS
        public static Balance From(BalanceResult balanceResult)
        {
            Balance result = new Balance();
            if(balanceResult.Money != null)
            {
                Money money = balanceResult.Money;

                DateTime recentDate;
                decimal cumulative = 0;
                int numOfDeals = 0;
                if(!DateTime.TryParse(money.RecentDate, out recentDate))
                {
                    Debug.WriteLine("Balance.From(...): Can't parse balanceResult.Money.RecentDate to DateTime.");
                }
                if(!decimal.TryParse(money.Cumulative, out cumulative))
                {
                    Debug.WriteLine("Balance.From(...): Can't parse balanceResult.Money.Cumulative to decimal.");
                }
                if(!int.TryParse(money.Deal, out numOfDeals))
                {
                    Debug.WriteLine("Balance.From(...): Can't parse balanceResult.Money.Deal to int.");
                }

                result.BalanceType = money.Type;
                result.RecentlyRebalancedDate = recentDate;
                result.Cumulative = cumulative;
                result.NumOfDeals = numOfDeals;
            }

            return result;
        }
        #endregion
    }
}
