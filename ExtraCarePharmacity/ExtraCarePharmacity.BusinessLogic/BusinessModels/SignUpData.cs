﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExtraCarePharmacity.DataAccess.ServiceModels;

namespace ExtraCarePharmacity.BusinessLogic.BusinessModels
{
    public class SignUpData
    {
        public string CustomerName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Street { get; set; }
        public string City { get; set; }

        #region ADAPTING METHODS
        public SignUpPostData ToPostData()
        {
            SignUpPostData postData = new SignUpPostData();

            postData.CustomerName = this.CustomerName;
            postData.Phone = this.Phone;
            postData.Email = this.Email;
            postData.Street = this.Street;
            postData.City = this.City;

            return postData;
        }
        #endregion
    }
}
