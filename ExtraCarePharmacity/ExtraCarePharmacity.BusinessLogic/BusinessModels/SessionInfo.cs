﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExtraCarePharmacity.DataAccess.ServiceModels;
using Kaliko;

namespace ExtraCarePharmacity.BusinessLogic.BusinessModels
{
    public class SessionInfo
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string FullName { get; set; }
        public string Session { get; set; }
        public string DefaultSession { get; set; }
        public string OTP { get; set; }
        public string CardNumber { get; set; }
        public bool Existed { get; set; }
        public bool IsConfirmed { get; set; }
        public bool IsValid { get; set; }
        public bool SignOut { get; set; }
        public DateTime LastConnected { get; set; }

        #region ADAPTING METHODS
        public static SessionInfo From(LoginResult loginResult)
        {
            SessionInfo result = new SessionInfo();

            DateTime lastConnected;
            if(!DateTime.TryParse(loginResult.LastConnected, out lastConnected))
            {
                Debug.WriteLine("SessionInfo.From(...): Can't parse loginResult.LastConnected to DateTime.");
            }

            result.Id = loginResult.Id;
            result.Email = loginResult.Email;
            result.PhoneNumber = loginResult.PhoneNumber;
            result.Session = loginResult.Session;
            result.DefaultSession = loginResult.DefaultSession;
            result.OTP = loginResult.OTP;
            result.CardNumber = loginResult.CardNumber;
            result.Existed = loginResult.Existed;
            result.IsConfirmed = loginResult.isConfirmed;
            result.IsValid = loginResult.isValid;
            result.SignOut = loginResult.signOut;
            result.LastConnected = lastConnected;

            return result;
        }
        #endregion
    }
}
