﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExtraCarePharmacity.DataAccess.ServiceModels;
using Kaliko;

namespace ExtraCarePharmacity.BusinessLogic.BusinessModels
{
    public class Store
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public string OpeningTime { get; set; }
        public string ClosingTime { get; set; }
        public string IsFMCG { get; set; }

        #region ADAPTING METHODS
        public static Store From(StoreResultDetail detail)
        {
            Store result = new Store();

            result.Id = detail.Id;
            result.Name = detail.Name;
            result.Address = detail.Address;
            result.Longitude = detail.Longitude;
            result.Latitude = detail.Latitude;
            result.OpeningTime = detail.OpenTime;
            result.ClosingTime = detail.CloseTime;
            result.IsFMCG = detail.IsFMCG;

            return result;
        }

        public static List<Store> From(StoreResult storeResult)
        {
            List<Store> result = new List<Store>();

            foreach(var item in storeResult)
            {
                result.Add(Store.From(item));
            }

            return result;
        }
        #endregion
    }
}
