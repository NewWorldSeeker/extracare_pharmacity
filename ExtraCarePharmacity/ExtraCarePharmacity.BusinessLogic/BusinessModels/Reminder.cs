﻿using ExtraCarePharmacity.DataAccess.ServiceModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtraCarePharmacity.BusinessLogic.BusinessModels
{
    public class Reminder
    {
        public string Id
        { get; set; }
        public string Name
        { get; set; }
        public string Address
        { get; set; }
        public double Latitude
        { get; set; }
        public double Longitude
        { get; set; }
        public string OpenTime
        { get; set; }
        public string IsFMCG
        { get; set; }

        #region ADAPTING METHODS
        public static Reminder From(ReminderResultDetail detail)
        {
            Reminder reminder = new Reminder();
            reminder.Id = detail.Id;
            reminder.Name = detail.Name;
            reminder.Address = detail.Address;
            try
            {
                reminder.Latitude = double.Parse(detail.Latitude);
                reminder.Longitude = double.Parse(detail.Longitude);
            }
            catch(Exception e)
            {
                Debug.WriteLine("Reminder.From(...): Can't parse latitude, longitude.");
                Debug.WriteLine(e.Message);
            }
            reminder.OpenTime = detail.OpenTime;
            reminder.IsFMCG = detail.IsFMCG;

            return reminder;
        }

        public static List<Reminder> From(ReminderResult result)
        {
            List<Reminder> reminders = new List<Reminder>();
            if(result.Reminder != null)
            {
                foreach(var item in result.Reminder)
                {
                    reminders.Add(Reminder.From(item));
                }
            }
            return reminders;
        }
        #endregion
    }
}
