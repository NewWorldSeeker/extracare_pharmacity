﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExtraCarePharmacity.DataAccess.ServiceModels;

namespace ExtraCarePharmacity.BusinessLogic.BusinessModels
{
    public class TransactionDetail
    {
        public string TransactionId { get; set; }
        public string ReceiptId { get; set; }
        public string ItemId { get; set; }
        public decimal Price { get; set; }
        public string TransDate { get; set; }
        public string TransTime { get; set; }
        public string Store { get; set; }
        public string Counter { get; set; }
        public string Unit { get; set; }
        public string StaffId { get; set; }
        public string SearchName { get; set; }
        public string FirstName { get; set; }

        public static TransactionDetail From(TransactionDetailResultDetail detail)
        {
            TransactionDetail result = new TransactionDetail();

            result.TransactionId = detail.TransactionId;
            result.ReceiptId = detail.ReceiptId;
            result.ItemId = detail.ItemId;
            try
            {
                result.Price = decimal.Parse(detail.Price);
            }
            catch(Exception e)
            {
                Debug.WriteLine("TransactionDetail.From(...): Can't parse detail.Price to decimal.");
                Debug.WriteLine(e.Message);
            }
            result.TransDate = detail.TransDate;
            result.TransTime = detail.TransTime;
            result.Store = detail.Store;
            result.Counter = detail.Counter;
            result.Unit = detail.Unit;
            result.StaffId = detail.StaffId;
            result.SearchName = detail.SearchName;
            result.FirstName = detail.FirstName;

            return result;
        }

        public static List<TransactionDetail> From(TransactionDetailResult detailResult)
        {
            List<TransactionDetail> result = new List<TransactionDetail>();

            foreach(var item in detailResult.List)
            {
                result.Add(TransactionDetail.From(item));
            }

            return result;
        }
    }
}
