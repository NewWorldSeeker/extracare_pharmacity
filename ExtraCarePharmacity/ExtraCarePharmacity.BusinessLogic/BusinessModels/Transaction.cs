﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using ExtraCarePharmacity.DataAccess.ServiceModels;
using Kaliko;

namespace ExtraCarePharmacity.BusinessLogic.BusinessModels
{
    public class Transaction
    {
        public string CardNumber { get; set; }
        public string ReceiptId { get; set; }
        public string TransactionId { get; set; }
        public string LoyaltyLevel { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public decimal Money { get; set; }

        #region ADTAPING METHODS
        public static Transaction From(TransactionResultDetail detail)
        {
            Transaction result = new Transaction();

            decimal money;
            if(!decimal.TryParse(detail.Money, out money))
            {
                Debug.WriteLine("Transaction.From(...): Can't parse detail.Money to decimal.");
            }

            result.CardNumber = detail.CardNumber;
            result.ReceiptId = detail.ReceiptId;
            result.TransactionId = detail.TransactionId;
            result.LoyaltyLevel = detail.Loyaltytier;
            result.Date = detail.Date;
            result.Time = detail.Time;
            result.Money = money;

            return result;
        }

        public static List<Transaction> From(TransactionResult result)
        {
            if (result.isConfirmed == false)
                return null;

            List<Transaction> transactions = new List<Transaction>();

            foreach(var item in result.List)
            {
                transactions.Add(Transaction.From(item));
            }

            return transactions;
        }
        #endregion
    }
}
